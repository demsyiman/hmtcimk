<?php
/*
Template Name: Blog Page
*/
?>
<?php get_header(); ?>
<!--#tp-section-wrap-->
<div id="tp-section-wrap" class="clearfix">
<!--#tp-section-left-or-right-->

	<div id="tp-section-left" class="eleven columns clearfix">	
 
 					 <h2 class="blogpost-tp-site-wrap-title"><?php _e('Recent Posts', 'silvermag');?></h2>	
							<div class="blog-lists-blog clearfix">
	 <div id="themepacific_infinite" class="blogposts-tp-site-wrap clearfix"> 
	<?php $temp = $wp_query; $wp_query= null;
		$wp_query = new WP_Query(); $wp_query->query('showposts=5' . '&paged='.$paged);
		 if (have_posts()) : while ($wp_query->have_posts()) : $wp_query->the_post(); ?>

<div class="blogposts-inner">
		<ul>	 		
			<li class="full-left clearfix">	
				<div class='magbig-thumb'>
					<span class="themepacific-cat-more" style="background:<?php echo silvermag_themepacific_show_catcolor($content='content');?>"> 
  				<?php $category = get_the_category(); 
				$brecat_title = $category[0]-> cat_ID;
				$category_link = get_category_link($brecat_title);
				echo '<a class="tpcrn-cat-more" href="'. esc_url( $category_link ) . '">' . $category[0]->cat_name . '</a>';
				 ?></span>
						 
					  <?php if ( has_post_thumbnail() ) { ?>
								<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="post-thumbnail">
								
								<?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'mag-image'); ?>
								<img src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>"  />
 		 
 							</a>
					  <?php } else { ?>
							<a href="<localhost:50000/hmtcbaru/wp-admin/images/posko.jpg" rel="bookmark" title="<?php the_title(); ?>"><img   src="<?php echo get_template_directory_uri(); ?>/images/default-image.png" width="60" height="60" alt="<?php the_title(); ?>" /></a>
						<?php } ?>
						 
   				</div>
						 
				<div class="list-block clearfix">
					<h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3> 	
							<div class="post-meta-blog">
					<span class="meta_author"> <?php the_author_posts_link(); ?></span>
					<span class="meta_date"> <?php the_time('F d, Y'); ?></span>
					<span class="meta_comments">  <a href="<?php comments_link(); ?>"><?php comments_number('0 Comment', '1 Comment', '% Comments'); ?></a></span>
					</div>
							
														
					<div class="maga-excerpt clearfix">
					<?php echo silvermag_themepacific_excerpt(18); ?>
					</div>	
 			</div>
		</li></ul>
	<br class="clear" />		
</div>	
 				<?php
				endwhile;
		 ?>
	 </div>				 
		<?php  else: ?>
 				<h2 class="noposts"><?php _e('Sorry, no posts to display!', 'silvermag'); ?></h2>
	 </div>
 <?php endif;?>
					  
</div>
		<div class="pagination clearfix">
			<?php silvermag_themepacific_tpcrn_pagination();   ?>
		</div> 
 
   	</div>
 	<!-- /blocks col -->
 <?php get_sidebar();  ?>
 <?php get_footer(); ?>
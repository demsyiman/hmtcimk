<?php
/*=====================
 TPCRN Custom Styles
 =====================*/
 global $themepacific_data;
 function silvermag_themepacific_wp_head() {
	global $themepacific_data;
	?>
	<style type="text/css" media="screen"> 
		<?php if(  !empty($themepacific_data['body_background_color']) || !empty($themepacific_data['custom_bg']) ) {?>

		body {
		<?php if(!empty($themepacific_data['body_background_color'])) {?>background-color:<?php echo $themepacific_data['body_background_color']; }?>;
		<?php if(!empty($themepacific_data['custom_bg']) && $themepacific_data['custom_full_bg_img'] == 'no')  {?>background-repeat: repeat;background-image: url('<?php if(!empty($themepacific_data['custom_bg_upload'])){echo $themepacific_data['custom_bg_upload'];}   else echo $themepacific_data['custom_bg'];?>');
		<?php }?>
		}
		<?php if(!empty($themepacific_data['custom_logo_margin_top'])) {?>
		#logo{margin-top:<?php echo $themepacific_data['custom_logo_margin_top'];?>}
		<?php }?>
		<?php } ?>
		<?php
		global $tpcrn_typo_options;	
		foreach( $tpcrn_typo_options as $name => $value){
		if($value['face'] == 'default'){
		unset($value['face']);
		}
		 if( !empty($value['color']) || !empty($value['face']) || !empty($value['style']) || !empty($value['size']) ){
			echo $name.'{ '; 
		  if( !empty($value['color']))echo "color: ". $value['color'].";";
		  if( !empty($value['face']))echo "font-family: ". $value['face'].";";
		  if( !empty($value['style']))echo "font-weight: ". $value['style'].";";
		  if( !empty($value['size']))echo "font-size: ". $value['size'].";";
		 echo '}';echo "\n";
		  }
		} /*for**/

		?>
 		<?php if(!empty($themepacific_data['custom_css'])) echo $themepacific_data['custom_css'];?>
		<?php 
			if ( has_nav_menu('mainNav') ){
				$menu_name ='mainNav';
				$locations = get_nav_menu_locations();
				$menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
				
				$menu_items = wp_get_nav_menu_items($menu->term_id);
				foreach ( (array) $menu_items as $key => $menu_item ) {
					if($menu_item->object == "category"){
				$O_IDS[] = $menu_item->object_id;
				}
			}
 	if(!empty($O_IDS)) {
			foreach($O_IDS as $O_ID) {
			$tpcrn_cat_clr = get_option("taxonomy_$O_ID");
			
			if(isset($tpcrn_cat_clr['color'])){
			$cat_clr = $tpcrn_cat_clr['color'];
			?>

			 #catnav.secondary >ul >li.menu-category-<?php echo $O_ID;?> a:hover,.archive.category-<?php echo $O_ID;?> #catnav.secondary ul li.current-menu-item {
			background: <?php echo $cat_clr;?>; }
			 #catnav ul li.menu-category-<?php echo $O_ID;?>.dropme a{background: <?php echo $cat_clr;?>; }
			 <?php 
			 
			} /*if*/
			} 
		} // isset
		}/*has mainNav*/
		?>
	 
	</style>
 
	<?php 
 
	if(!empty($themepacific_data['custom_bg_upload']) && $themepacific_data['custom_full_bg_img'] == 'yes'){?>
	<script type="text/javascript">
					 jQuery(document).ready(function(){
						jQuery.backstretch("<?php echo $themepacific_data['custom_bg_upload'];?>");
					   });
					</script>
	<?php } 
}

if(isset($themepacific_data['body_font'])){
 $tpcrn_typo_options = array(
	"body" 		    =>		$themepacific_data['body_font'],
	"#catnav ul a"  =>		$themepacific_data['headings_topmenu_font'],
	"#catnav.secondary ul a"  =>		$themepacific_data['headings_catmenu_font'],
	"h2.blogpost-tp-site-wrap-title"   =>		$themepacific_data['blog_mag_title_font'],
	".blog-lists-blog h3 a,.article-header h3 a"  =>		$themepacific_data['blog_post_title_font'],
	" .blog-lists h3 a"    =>		$themepacific_data['mag_post_title_font'],
	".widget-head, ul.tabs2 li a"         =>		$themepacific_data['sb_widget_title_font'],
	"h3.widget-title"      =>		$themepacific_data['fb_widget_title_font'],
	".post-title h1"       =>		$themepacific_data['post_title_font'],
	"#post-meta ,.post-meta-blog"  =>		$themepacific_data['post_meta_font'],
	".post_content"  			   =>		$themepacific_data['post_content_font'],
 	".single-navigation, .comment-nav-above, .comment-nav-below"  =>$themepacific_data['post_navigation_font'],
	".comment-content"  =>		$themepacific_data['post_comment_font'],
	".post_content h1"  =>		$themepacific_data['post_h1_font'],
	".post_content h2"  =>		$themepacific_data['post_h2_font'],
	".post_content h3"  =>		$themepacific_data['post_h3_font'],
	".post_content h4"  =>		$themepacific_data['post_h4_font'],
	".post_content h5"  =>		$themepacific_data['post_h5_font'],
	".post_content h6"  =>		$themepacific_data['post_h6_font'],
 
); 
} 
 
 
/**
 * Filters wp_title to print a neat <title> tag based on what is being viewed.
 */
function silvermag_themepacific_wp_title( $title, $sep ) {
	global $page, $paged;

	if ( is_feed() )
		return $title;

	// Add the blog name
	$title .= get_bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title .= " $sep $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		$title .= " $sep " . sprintf( __( 'Page %s', 'silvermag' ), max( $paged, $page ) );

	return $title;
}
add_filter( 'wp_title', 'silvermag_themepacific_wp_title', 10, 2 ); 


//Category Menu

function silvermag_themepacific_wpa_category_nav_classs( $classes, $item ){
    if( 'category' == $item->object ){
        $classes[] = 'menu-category-' . $item->object_id;
    }
    return $classes;
}
add_filter( 'nav_menu_css_class', 'silvermag_themepacific_wpa_category_nav_classs', 10, 2 );

  function silvermag_wp_login_form( $login_only  = 0 ) {
	global $user_ID, $user_identity, $user_level;
	
	if ( $user_ID ) : ?>
		<?php if( empty( $login_only ) ): ?>
		<div id="tpcrn-user-in">
			<p class="tpcrn-user-greet"><?php _e( 'Howdy!' , 'silvermag' ) ?> <strong><?php echo $user_identity ?></strong></p>
			<div class="tpcrn-author-avatar"><?php echo get_avatar( $user_ID, $size = '90'); ?></div>
			<ul>
				<li><a href="<?php echo home_url() ?>/wp-admin/"><?php _e( 'Dashboard' , 'silvermag' ) ?> </a></li>
				<li><a href="<?php echo home_url() ?>/wp-admin/profile.php"><?php _e( 'Your Profile' , 'silvermag' ) ?> </a></li>
				<li><a href="<?php echo wp_logout_url(); ?>"><?php _e( 'Logout' , 'silvermag' ) ?> </a></li>
			</ul>
 			<div class="clear"></div>
		</div>
		<?php endif; ?>
	<?php else: ?>
		<div id="themepacific-login-form">
			<form action="<?php echo home_url() ?>/wp-login.php" method="post">
				<p id="tpcrn-username"><input size="40" type="text" name="log" value="<?php _e( 'Username' , 'silvermag' ) ?>" onfocus="if (this.value == '<?php _e( 'Username' , 'silvermag' ) ?>') {this.value = '';}" onblur="if (this.value == '') {this.value = '<?php _e( 'Username' , 'silvermag' ) ?>';}"   /></p>
				<p id="tpcrn-pw"><input size="40" type="password" name="pwd" value="<?php _e( 'Password' , 'silvermag' ) ?>" onfocus="if (this.value == '<?php _e( 'Password' , 'silvermag' ) ?>') {this.value = '';}" onblur="if (this.value == '') {this.value = '<?php _e( 'Password' , 'silvermag' ) ?>';}"  /></p>
				<input type="submit" name="submit" value="<?php _e( 'Log in' , 'silvermag' ) ?>" class="nletterbutton" />
				<label><input name="remembercheck" type="checkbox" checked="checked" value="forever" /> <?php _e( 'Remember Me' , 'silvermag' ) ?></label>
				<input type="hidden" name="redirect_to" value="<?php echo $_SERVER['REQUEST_URI']; ?>"/>
			</form>
			<ul class="tpcrn-forget-pw">
				<?php if ( get_option('users_can_register') ) : ?> <li><a href="<?php echo home_url() ?>/wp-register.php"><?php _e( 'Register' , 'silvermag' ) ?></a></li><?php endif; ?>
				<li><a href="<?php echo home_url() ?>/wp-login.php?action=lostpassword"><?php _e( 'Lost your password?' , 'silvermag' ) ?></a></li>
			</ul>
		</div>
	<?php endif;
}

function silvermag_twitter_follow_count($twitter_id) {
	global $themepacific_data;
	require_once(TEMPLATEPATH . '/includes/twitteroauth/twitteroauth.php');
 		$twitter_username 		= $twitter_id;
		$consumer_key 			=  $themepacific_data['twit_c_key'];
		$consumer_secret		=  $themepacific_data['twit_c_secret'];
		$access_token 			=  $themepacific_data['twit_a_token'];
		$access_token_secret 	=  $themepacific_data['twit_a_token_secret'];
		
		$twitterConnection = new TwitterOAuth( $consumer_key , $consumer_secret , $access_token , $access_token_secret	);
		$twitterData = $twitterConnection->get('users/show', array('screen_name' => $twitter_username));
 		$twitter['followers_count'] = $twitterData->followers_count;
		
 	if( get_option( 'followers_count') < $twitter['followers_count'] )
		update_option( 'followers_count' , $twitter['followers_count'] );
		
	if( $twitter['followers_count'] == 0 && get_option( 'followers_count') )
		$twitter['followers_count'] = get_option( 'followers_count');
			
	elseif( $twitter['followers_count'] == 0 && !get_option( 'followers_count') )
		$twitter['followers_count'] = 0;
	
	return $twitter;
}
 
function silvermag_themepacific_get_data_fb($json_url,$id){
   $sharestransient = 'followers_tpcrn_url_fb'.$id;
   $cachedresult =  get_transient($sharestransient);
  if ($cachedresult !== false ) {
	return $cachedresult;
	
	 } else {
  		$jsondoc = wp_remote_get($json_url);
		if( !is_wp_error( $jsondoc ) ) {
			// parameter 'true' is necessary for output as PHP array
			$json_data = json_decode($jsondoc['body'],true);
			$result = $json_data['likes'];	
		
			set_transient($sharestransient, $result, 60*60*4);
			update_option($sharestransient, $result);		
			 return $result;  
		} else{
			return $jsondoc->get_error_message();
			return;
		}
  	}
 }
 /*-- Breadcrumbs--*/
 function silvermag_themepacific_breadcrumb() {
	if (!is_home()) {
	
		echo '<ul id="tpcrn-breadcrumbs"><li><a href="'.home_url().'">Home &raquo;</a> </li>';
		if (is_category() || is_single()) {
			 
$category = get_the_category(); 
$brecat_title = $category[0]-> cat_ID;
$category_link = get_category_link($brecat_title);
echo '<li><a class="vca" href="'. esc_url( $category_link ) . '">' . $category[0]->cat_name . ' &raquo;</a></li>';
 	 
			if (is_single()) {
				echo '<li class="current">';
				the_title();
				echo '</li>';
			}
		} elseif (is_page()) {
			echo '<li class="current">';
				the_title();
				echo '</li>';
		}
	echo '</ul>'; 
	}
}
 
/*-- Pagination --*/

function silvermag_themepacific_tpcrn_pagination() {
	
		global $wp_query;
		$big = 999999999;
		echo paginate_links( array(
			'base' => str_replace( $big, '%#%', get_pagenum_link( $big ) ),
			'format' => '?paged=%#%',
			'prev_next'    => false,
			'prev_text'    => __('<i class="icon-double-angle-left"></i>'),
	        'next_text'    => __('<i class="icon-double-angle-right"></i>'),
			'current' => max( 1, get_query_var('paged') ),
			'total' => $wp_query->max_num_pages )
		);
	}
 
  /*-- Custom Excerpts--*/
function silvermag_themepacific_custom_read_more() {
    return '... <div class="themepacific-read-more"><a class="tpcrn-read-more" href="'.get_permalink(get_the_ID()).'">'. __('Read more&nbsp;&raquo;','silvermag').'</a></div>';
} 
function silvermag_themepacific_excerpt($limit) {
    return wp_trim_words(get_the_excerpt(), $limit, silvermag_themepacific_custom_read_more());
}
/*-- cat color --*/
function silvermag_themepacific_show_catcolor($type,$c_id=''){
			
 				if($type=='content'){
					$category = get_the_category();
					$t_id = get_cat_ID($category[0]->cat_name);
							}
				if($type=='block'){
					$t_id = $c_id;
				}
 				 
				$tpcrn_arc_clr = get_option("taxonomy_$t_id");
				if(isset($tpcrn_arc_clr['color'])){
					return $tpcrn_arc_clr['color'];
				}
 }				
function silvermag_themepacific_tpcrn_shw_category(){

	$categories = get_the_category();
	$classes = '';
	if($categories){
	$c=1;
	   foreach($categories as $category) {
		   $cat_id = $category->term_id;
		   $ca_color = silvermag_themepacific_show_catcolor($type='block', $cat_id);
		   if($c>1) echo ", ";
			echo '<a style="color:'.$ca_color.'" href="'.get_category_link( $cat_id ).'">'.get_cat_name( $cat_id ).'</a>';
			
	  $c++;
	  }
	}
}
if (!isset( $content_width )) $content_width = 620;
/*-- Multiple Page Nav--*/		

function silvermag_themepacific_single_split_page_links($args) {

    global $page, $numpages, $more, $pagenow;

    if (!$args['next_or_number'] == 'next_and_number') 
        return $args; # exit early

    $args['next_or_number'] = 'number'; # keep numbering for the main part
    if (!$more)
        return $args; # exit early

    if($page-1) # there is a previous page
        $args['before'] .='<span class="tpcrn-sp-pre">'.  _wp_link_page($page-1)
            . $args['link_before']. $args['previouspagelink'] . $args['link_after'] . '</a></span>'
        ;

    if ($page<$numpages) # there is a next page
        $args['after'] ='<span class="tpcrn-sp-next">'.  _wp_link_page($page+1)
            .$args['link_before'] . $args['nextpagelink'] . $args['link_after'] . '</a></span>'
            . $args['after']
        ;

    return $args;
	}
	
	
function silvermag_themepacific_wp_link_pages_tp(){	
	 wp_link_pages(array(
	'before' => '<div class="single-split-page"><p>',
	'after' => '</p></div>',
	'next_or_number' => 'next_and_number', # activate parameter overloading
    'nextpagelink' => __('Next','silvermag'),
    'previouspagelink' => __('Previous','silvermag'),
	'pagelink' => '%',
	'echo' => 1)
    );
	}
 
// Add term page
function silvermag_themepacific_taxonomy_add_new_meta_field() {
	// this will add the custom meta field to the add new term page
	?>
 <tr class="form-field">
  <th scope="row" valign="top"><label for="inp-color">Pick Color</label></th>
	<td>
		<input type="text" value="#fff" name="term_meta[color]" id="inp-color" class="tpcrn-pick-clr post-form" data-default-color="#fff" />
		
	 
	 <p class="description">Select color for category?</p>
	</td>
</tr>
<script type='text/javascript'>  
    jQuery(document).ready(function($) {  
        $('.tpcrn-pick-clr').wpColorPicker();  
    });  
</script>		
<?php
}
 
function silvermag_themepacific_load_color_picker() {      
    wp_enqueue_style( 'wp-color-picker' );          
    wp_enqueue_script( 'wp-color-picker' );      
}
 
// Edit term page
function silvermag_themepacific_taxonomy_edit_meta_field($term) {
 
	// put the term ID into a variable
	$t_id = $term->term_id;
 
	// retrieve the existing value(s) for this meta field. This returns an array
	$term_meta = get_option( "taxonomy_$t_id" ); 

?> 
 
<tr class="form-field">
			<th scope="row" valign="top"><label for="inp-color">Pick Color</label></th>
			<td>
	<input type="text" value="<?php if(isset($term_meta['color'])){ echo esc_attr( $term_meta['color'] ) ? esc_attr( $term_meta['color'] ) : ''; } ?>" name="term_meta[color]" id="inp-color" class="tpcrn-pick-clr post-form" data-default-color="#fff" />	
	 
	 <p class="description">Select color for category</p>
	</td>
</tr>

<script type='text/javascript'>  
    jQuery(document).ready(function($) {  
        $('.tpcrn-pick-clr').wpColorPicker();  
    });  
</script>
 
<?php
}
 
 
// Save extra taxonomy fields callback function.
function silvermag_themepacific_save_taxonomy_custom_meta( $term_id ) {
	if ( isset( $_POST['term_meta'] ) ) {
		$t_id = $term_id;
		$term_meta = get_option( "taxonomy_$t_id" );
		$cat_keys = array_keys( $_POST['term_meta'] );
		foreach ( $cat_keys as $key ) {
			if ( isset ( $_POST['term_meta'][$key] ) ) {
				$term_meta[$key] = $_POST['term_meta'][$key];
			}
		}
		// Save the option array.
		update_option( "taxonomy_$t_id", $term_meta );
	}
}  

	
 
/*===================================================================================*/
/*  Comments
/*==================================================================================*/

function silvermag_themepacific_themepacific_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
	?>
	<li class="post pingback">
		<p><?php _e( 'Pingback:', 'silvermag' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __( 'Edit', 'silvermag' ), '<span class="edit-link">', '</span>' ); ?></p>
	<?php
			break;
		default :
	?>
	<li id="comment-<?php comment_ID(); ?>">
		<div <?php comment_class('comment-tp-site-wrap'); ?> >
 				<div class="comment-avatar">
					<?php
						$avatar_size = 65;
						if ( '0' != $comment->comment_parent )
							$avatar_size = 65;

						echo get_avatar( $comment, $avatar_size );?>
				</div>
				<!--comment avatar-->
				<div class="comment-meta">
					<?php	
						printf( __( '%1$s  %2$s  ', 'silvermag' ),
							sprintf( '<div class="author">%s</div>', get_comment_author_link() ),
							sprintf( '%4$s<a href="%1$s"><span class="time" style="border:none;">%3$s</span></a>',
								esc_url( get_comment_link( $comment->comment_ID ) ),
								get_comment_time( 'c' ),get_comment_date(),								
								sprintf( __( '<span class="time">%1$s </span>', 'silvermag' ),   get_comment_time() )
							)
						);
					?>

					<?php edit_comment_link( __( 'Edit', 'silvermag' ), '<span class="edit-link">', '</span>' ); ?>
				</div><!-- /comment-meta -->

				<?php if ( $comment->comment_approved == '0' ) : ?>
					<em class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'silvermag' ); ?></em>
					<br />
				<?php endif; ?>
 			<div class="comment-content">
				<?php comment_text(); ?>
				<div class="reply">
					<?php comment_reply_link( array_merge( $args, array( 'reply_text' => __( ' <span><i class="icon-reply"></i></span> Reply', 'silvermag' ), 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
				</div> <!--/reply -->
			</div><!--/comment-content -->	
		</div>	<!--/Comment-tp-site-wrap -->
 			 
 	<?php
			break;
	endswitch;
}
?>

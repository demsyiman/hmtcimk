<?php
/*
Author: RAJA CRN
URL: ThemePacific.com
Theme Name:  Silver Mag Pro
*/

/*===================================================================================*/
/*  Load ThemePacific FrameWork Assets
/*==================================================================================*/

		define('TPACIFIC_DIR', get_template_directory());
		define('TPACIFIC_URI', get_template_directory_uri());	    
		define('TPACIFIC_ADMIN', TPACIFIC_DIR . '/admin');
		define('TPACIFIC_ADMINURI', TPACIFIC_URI . '/admin');          
		define('TPACIFIC_JS', TPACIFIC_URI . '/js'); 
		define('TPACIFIC_CSS', TPACIFIC_URI . '/css');
		define('TPACIFIC_IMG', TPACIFIC_URI . '/images');
  		define('TPACIFIC_WIDGET', TPACIFIC_ADMIN . '/widgets');
 		include_once (TPACIFIC_ADMIN.'/index.php');
 		include_once (TPACIFIC_DIR.'/custom_fn/tpcrn_functions.php');

$themename="silvermag";
$themefolder = "silvermag";

define ('theme_name', $themename );
define ('theme_ver' , 1.0  );
 
// Notifier Info
$notifier_name = $themename;
$notifier_url = "http://demo.themepacific.com/xml/".$themefolder.".xml";


 
// Constants for the theme name, folder and remote XML url
define( 'MTHEME_NOTIFIER_THEME_NAME', $themename );
define( 'MTHEME_NOTIFIER_THEME_FOLDER_NAME', $themefolder );
define( 'MTHEME_NOTIFIER_XML_FILE', $notifier_url );
define( 'MTHEME_NOTIFIER_CACHE_INTERVAL', 1 );
include (TPACIFIC_ADMIN . '/notifier/update-notifier.php');
 
/*===================================================================================*/
/* Theme Support
/*==================================================================================*/

/*-- Post thumbnail + Menu Support + Formats + Feeds --*/
function silvermag_themepacific_theme_support_image()
{
 
 		add_theme_support('post-thumbnails' );
		add_image_size('mag-image', 340, 160,true);
 		add_image_size('blog-image', 220, 180,true);		
		add_image_size('sb-post-thumbnail', 70, 70,true);
		add_image_size('sb-post-big-thumbnail', 376, 192,true);
		add_image_size('sb-post-small-thumbnail', 188, 144,true);
		add_image_size('feat-slider', 764, 336,true);
		add_theme_support( 'automatic-feed-links' );
 		load_theme_textdomain( 'silvermag', get_template_directory() . '/languages' );

 			register_nav_menus(
			array(
 				
 				'mainNav' => __('Secondary Menu','silvermag' ),
 				'mainNav2' => __('Secondary Menu 2','silvermag' ),
			)		
		);
	
 }
 add_action( 'after_setup_theme', 'silvermag_themepacific_theme_support_image' );

 add_theme_support( 'infinite-scroll', array(
	'type'           => 'click',
    'container'  => 'themepacific_infinite',
 
    'footer'     => 'false',
));
 
/*===================================================================================*/
/* Functions
/*==================================================================================*/

/*-- Load Custom Theme Scripts using Enqueue --*/
function silvermag_themepacific_tpcrn_scripts_method() {
	if ( !is_admin() ) {
		global $themepacific_data;
		global $tpcrn_typo_options;
		$sys_fonts = array('default', 'arial','trebuchet','Times New Roman','verdana','georgia','tahoma','palatino','helvetica');$foi=0;
		foreach( $tpcrn_typo_options as $name => $value){
			$theme_fonts[] = $value['face'];
		  }
			$theme_fonts_uValue=array_diff($theme_fonts,$sys_fonts); 
			$theme_gfonts = array_unique($theme_fonts_uValue);
		foreach($theme_gfonts as $theme_gfont){
			 $g_web_font = 'http://fonts.googleapis.com/css?family='.str_replace(' ', '+', $theme_gfont).'';
			 wp_enqueue_style( 'google-web-font'.$foi.'', ''.$g_web_font.'', array(), NULL);
			 $foi++; 
		   }
        wp_enqueue_style( 'style', get_stylesheet_uri());  
		/** Two Columns*/
		
  		if( $themepacific_data['tp_3cols'] == '0' ){
            wp_enqueue_style('silvermag_2col', get_stylesheet_directory_uri().'/css/silvermag_2col.css');
			}		
 		wp_enqueue_style('skeleton', get_stylesheet_directory_uri().'/css/skeleton.css');
		wp_enqueue_style('flex', get_stylesheet_directory_uri().'/css/flexslider.css');
		wp_enqueue_style('tpcrnskin', get_stylesheet_directory_uri().'/css/skins/default.css');
   		wp_register_script('easing', get_template_directory_uri(). '/js/jquery.easing.1.3.js'); 
  		wp_register_script('ticker', get_template_directory_uri(). '/js/jquery.webticker.min.js'); 
  		wp_register_script('jquery.backstretch.min', get_template_directory_uri(). '/js/jquery.backstretch.min.js'); 
  		wp_register_script('flexslider', get_template_directory_uri(). '/js/jquery.flexslider-min.js'); 
  		wp_register_script('themepacific.script', get_template_directory_uri(). '/js/tpcrn_scripts.js', array('jquery'), '1.0', true); 	
 	
 		   $protocol = is_ssl() ? 'https' : 'http';
		$query_args = array(
		   'family' => 'Oswald|PT+Sans|Open+Sans',
 		  
		);


		wp_enqueue_style('google-webfonts',
        add_query_arg($query_args, "$protocol://fonts.googleapis.com/css" ),
        array(), null);
		wp_enqueue_script('jquery');
		wp_enqueue_script('camera');
		wp_enqueue_script('flexslider');
     	wp_enqueue_script('jquery-ui-widget');	
		wp_enqueue_script('jquery.backstretch.min');
   		wp_enqueue_script('easing');
  		wp_enqueue_script('ticker');
  		
		wp_enqueue_script('themepacific.script');
		  			if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
 	
	}

}
 
 

function add_ie_html5_shim () {
    echo '<!--[if lt IE 9]>';
    echo '<script src="'.get_template_directory_uri(). '/js/html5.js"></script>';
    echo '<script src="'.get_template_directory_uri(). '/js/css3-mediaqueries.js"></script>';
     echo '<![endif]-->';
}
add_action('wp_head', 'add_ie_html5_shim');
 /*-----------------------------------------------------------------------------------*/
/* Register sidebars
/*-----------------------------------------------------------------------------------*/
 function silvermag_themepacific_widgets_init() {

 	register_sidebar(array(
		'id' => 'sidebar-1',
		'name' => 'Sidebar Small',
		'before_widget' => '<div id="%1$s" class="sidebar-widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-head">',
		'after_title' => '</h3>',
	)); 	
	register_sidebar(array(
		'id' => 'sidebar-2',
		'name' => 'Sidebar Big',
		'before_widget' => '<div id="%1$s" class="sidebar-widget %2$s">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-head">',
		'after_title' => '</h3>',
	));
	
	register_sidebar(array(
		'id' => 'sidebar-3',
		'name' => 'Magazine Style Widgets',
		'before_widget' => '<div id="%1$s" class="%2$s blogposts-tp-site-wrap clearfix">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));	
	register_sidebar(array(
		'id' => 'sidebar-4',
		'name' => 'Magazine Style Widgets2',
		'before_widget' => '<div id="%1$s" class="%2$s blogposts-tp-site-wrap clearfix">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));
	register_sidebar(array(
		'id' => 'sidebar-5',
		'name' => 'Full Width Widgets Home',
		'before_widget' => '<div id="%1$s" class="%2$s blogposts-tp-site-wrap clearfix">',
		'after_widget' => '</div>',
		'before_title' => '<h3>',
		'after_title' => '</h3>',
	));
 
	
	register_sidebar(array(
		'id' => 'sidebar-6',
		'name' => 'Footer Block 1',
		'before_widget' => '<div id="%1$s" class="%2$s widget">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	));
	
	register_sidebar(array(
		'id' => 'sidebar-7',
		'name' => 'Footer Block 2',
		'before_widget' => '<div id="%1$s" class="%2$s widget">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	));
	
	register_sidebar(array(
		'id' => 'sidebar-8',
		'name' => 'Footer Block 3',
		'before_widget' => '<div id="%1$s" class="%2$s widget">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	));
	register_sidebar(array(
		'id' => 'sidebar-9',
		'name' => 'Footer Block 4',
		'before_widget' => '<div id="%1$s" class="%2$s widget">',
		'after_widget' => '</div>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	));
}
add_action( 'widgets_init', 'silvermag_themepacific_widgets_init' );

/*===================================================================================*/
/*  Actions + Filters + Translation
/*==================================================================================*/

 /*-- Add Custom Styles--*/ 
add_action('wp_head', 'silvermag_themepacific_wp_head');
  
/*-- Multiple Page Nav tweak --*/		
add_filter('wp_link_pages_args','silvermag_themepacific_single_split_page_links');
  
/*-- Register and enqueue  javascripts--*/
add_action('wp_enqueue_scripts', 'silvermag_themepacific_tpcrn_scripts_method');
add_action( 'silvermag_themepacific_tpcrn_cre_def_call', 'silvermag_themepacific_tpcrn_cre_def');
add_action( 'category_edit_form_fields', 'silvermag_themepacific_taxonomy_edit_meta_field', 10, 2 );
add_action( 'category_edit_form_fields', 'silvermag_themepacific_load_color_picker' );  
add_action( 'edited_category', 'silvermag_themepacific_save_taxonomy_custom_meta', 10, 2 );  
add_action( 'create_category', 'silvermag_themepacific_save_taxonomy_custom_meta', 10, 2 );
add_action( 'category_add_form_fields', 'silvermag_themepacific_taxonomy_add_new_meta_field', 10, 2 );
add_action( 'category_add_form_fields', 'silvermag_themepacific_load_color_picker' );
?>
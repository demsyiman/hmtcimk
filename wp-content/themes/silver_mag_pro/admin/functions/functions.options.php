<?php

add_action('init','silvermag_themepacific_of_options');

if (!function_exists('silvermag_themepacific_of_options'))
{
	function silvermag_themepacific_of_options()
	{ 
	  	$framework = 'tpcrn_';
		function silvermag_themapacific_tpcrn_strcasecmp_name( $a, $b ) {
		return strcasecmp( $a->name, $b->name );
		}
 		//Access the WordPress Categories via an Array
		$of_categories = array();  
		$of_categories_obj = get_categories('hide_empty=0');
		foreach ($of_categories_obj as $of_cat) {
		    $of_categories[$of_cat->cat_ID] = $of_cat->cat_name;
			}
		
		$categories_tmp 	= array_unshift($of_categories, "Recent Posts");    
	    $categories_tmp 	= array_unshift($of_categories, "Select a category:");
		//Access the WordPress Tags via an Array
				$tpcrn_tags = Array();
				$tags = get_tags( array( 'number' => 1000, 'orderby' => 'count', 'order' => 'DESC' ) );
				usort( $tags, 'silvermag_themapacific_tpcrn_strcasecmp_name');

				foreach ( $tags as $tag ) {
 				$tpcrn_tags[$tag->term_id] = $tag->name;
				}   
		   $tags_tmp 	= array_unshift($tpcrn_tags, "Select a Tag:");    
		   
		   
		//Access the WordPress Pages via an Array
		$of_pages 			= array();
		$of_pages_obj 		= get_pages('sort_column=post_parent,menu_order');    
		foreach ($of_pages_obj as $of_page) {
		    $of_pages[$of_page->ID] = $of_page->post_name; }
		$of_pages_tmp 		= array_unshift($of_pages, "Select a page:");       
	
		//Testing 
		$of_options_select 	= array("one","two","three","four","five"); 
		$of_options_radio 	= array("one" => "One","two" => "Two","three" => "Three","four" => "Four","five" => "Five");
		
		//Sample Homepage blocks for the layout manager (sorter)
		$of_options_homepage_blocks = array
		( 
			"enabled" => array (
				"placebo" 			=> "placebo", //REQUIRED!
 				"hb_nor_blog"		=> "Recent Posts",
 				"hb_mag_1"			=> "Magazine Block ",				
 				"hb_mag_2"			=> "Magazine Block 2 ",				
 				
			), 
			"disabled" => array (
				"placebo" => "placebo", //REQUIRED!
  				
			),
		);


		//Stylesheets Reader
		$alt_stylesheet_path = LAYOUT_PATH;
		$alt_stylesheets = array();
		
		if ( is_dir($alt_stylesheet_path) ) 
		{
		    if ($alt_stylesheet_dir = opendir($alt_stylesheet_path) ) 
		    { 
		        while ( ($alt_stylesheet_file = readdir($alt_stylesheet_dir)) !== false ) 
		        {
		            if(stristr($alt_stylesheet_file, ".css") !== false)
		            {
		                $alt_stylesheets[] = $alt_stylesheet_file;
		            }
		        }    
		    }
		}


		//Background Images Reader
		$bg_images_path = get_stylesheet_directory(). '/images/bg/'; // change this to where you store your bg images
		$bg_images_url = get_template_directory_uri().'/images/bg/'; // change this to where you store your bg images
		$bg_images = array();
		
		if ( is_dir($bg_images_path) ) {
		    if ($bg_images_dir = opendir($bg_images_path) ) { 
		        while ( ($bg_images_file = readdir($bg_images_dir)) !== false ) {
		            if(stristr($bg_images_file, ".png") !== false || stristr($bg_images_file, ".jpg") !== false) {
		                $bg_images[] = $bg_images_url . $bg_images_file;
		            }
		        }    
		    }
		}
		

		/*-----------------------------------------------------------------------------------*/
		/* TO DO: Add options/functions that use these */
		/*-----------------------------------------------------------------------------------*/
		
		//More Options
		$uploads_arr 		= wp_upload_dir();
		$all_uploads_path 	= $uploads_arr['path'];
		$all_uploads 		= get_option('of_uploads');
		$other_entries 		= array("Select a number:","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19");
		$body_repeat 		= array("no-repeat","repeat-x","repeat-y","repeat");
		$body_pos 			= array("top left","top center","top right","center left","center center","center right","bottom left","bottom center","bottom right");
		
		// Image Alignment radio box
		$of_options_thumb_align = array("alignleft" => "Left","alignright" => "Right","aligncenter" => "Center"); 
		
		// Image Links to Options
		$of_options_image_link_to = array("image" => "The Image","post" => "The Post"); 

 	  
 
/*-----------------------------------------------------------------------------------*/
/* The Options Array */
/*-----------------------------------------------------------------------------------*/

// Set the Options Array
global $of_options;
$of_options = array();
$of_options[] = array( "name" => __('General Settings','silvermag'),
                    "type" => "heading");
$of_options[] = array( "name" => "General",
					"desc" => "",
					"id" => "introduction",
					"std" => __('<h3>General Settings</h3>','silvermag'),
					"icon" => true,
					"type" => "info");
					
					
$of_options[] = array( "name" => __('Custom Favicon','silvermag'),
					"desc" =>  __('Upload a 16px x 16px Png/Gif image that will represent your website favicon','silvermag'),
					"id" => "custom_favicon",
					"std" => "",
					"type" => "media"); 
$logo = get_template_directory_uri() . '/images/logo.png';					
$of_options[] = array( "name" => __('Custom Logo','silvermag'),
					"desc" => __('Upload a Png/Gif image that will represent your website Logo.','silvermag'),
					"id" => "custom_logo",
					"std" => $logo,
					"type" => "media"); 
 					
$of_options[] = array( "name" => __('Custom Feed URL','silvermag'),
					"desc" => __('Enter Feedburner URL or Other','silvermag'),
					"id" => "custom_feedburner",
					"std" => "",
					"type" => "text"); 					
                                               
$of_options[] = array( "name" => __('Tracking Code','silvermag'),
					"desc" => __('Paste your Google Analytics (or other) tracking code here. Dont forget to add script tags,if not in the code</br><small> &lt;script&gt; ...,.. &lt;/script&gt; </small>','silvermag'),
					"id" => "google_analytics",
					"std" => "",
					"type" => "textarea");

$of_options[] = array( "name" => __('Layout Columns', 'silvermag'),
					"desc" => __('2 columns or 3 columns', 'silvermag'),
					"id" => "tp_3cols",
					"std" => 1,
          			"folds" => 1,
					"on" 		=> "3 Columns",
					"off" 		=> "2 Columns",
					"type" => "switch"
					); 					
$of_options[] = array( "name" => __('Show Footer Widgets', 'silvermag'),
					"desc" => __('Select to show the Footer Widgets.', 'silvermag'),
					"id" => "shw_footer_widg",
					"std" => "yes",
					"type" => "select",
					"options" => array('yes'=>'Yes','no'=>'No'));
$of_options[] = array( "name" =>__('Footer Text','silvermag'),
                    "desc" => __('Add the Copyright text info.','silvermag'),
                    "id" => "cus_footer_text",
                    "std" => "",
                    "type" => "textarea");   
 
 
					
$of_options[] = array( "name" => __('Home Settings','silvermag'),
					"type" => "heading");
 
 					
$of_options[] = array( "name" => "Hello there!",
					"desc" => "",
					"id" => "introduction",
					"std" => __('<h3>Home Page Content Organizer</h3>','silvermag'),
					"icon" => true,
					"type" => "info");

 					
$of_options[] = array( "name" => __('Homepage Content Layout Manager','silvermag'),
					"desc" => __('Organize how you want the layout to appear on the homepage','silvermag'),
					"id" => "homepage_blocks_content",
					"std" => $of_options_homepage_blocks,
					"type" => "sorter");


$of_options[] = array( "name" => __('BlogPosts Display Style ', 'silvermag'),
					"desc" => __('Select style', 'silvermag'),
					"id" => "blog_style",
					"std" => "grid",
					"type" => "select",
					"options" => array(
 					'grid'=>'Grid Style',
 					'traditional'=>'Traditional',
 					));
					
$of_options[] = array( "name" =>__('News Ticker Title','silvermag'),
					"desc" => 'Enter Title for Ticker in the Header',
					"id" => "ticker_title",					 
					"std" => "Breaking News",
					"type" => "text");						
$of_options[] = array( "name" => __('Breaking News Ticker','silvermag'),
					"desc" => __('Select category for Breaking News Ticker.','silvermag'),
					"id" => "ticker_category",
					"std" =>"",
 					"type" => "select",
					"options" => $of_categories);
$of_options[] = array( "name" =>__('Enter Number of  News Ticker Posts','silvermag'),
					"desc" => '',
					"id" => "ticker_post_no",					 
					"std" => "3",
					"type" => "text");


$of_options[] = array( "name" => __('HomePage Slider','themeapacific'),
					"desc" => __('Show HomePage slider Options','silvermag'),
					"id" => "offline_feat_slide",
					"std" => 0,
          			"folds" => 1,
					"type" => "switch"					
					);  
 
 $of_options[] = array( "name" => __('Select a Category or use Theme Slider options','imagmag'),
					"desc" => __('Select category for slider. If you want to use theme slider options,then leave it to default option.','imagmag'),
					"id" => "feat_slide_category",
					"std" => "0",
					"fold" => "offline_feat_slide",  
					"type" => "select",
					"options" => $of_categories);

$of_options[] = array( "name" =>__('Theme Slider Options, use Page IDs','silvermag'),
					"desc" => 'Enter Page IDs to show in slider Posts',
					"id" => "page_id_slider",					 
					"std" => "",
					"type" => "text");					

$of_options[] = array( "name" => __('Slider Transition ', 'silvermag'),
					"desc" => __('Select Slider transition', 'silvermag'),
					"id" => "feat_slide_trans",
					"std" => "random",
					"type" => "select",
					"options" => array(
 					'simplefade'=>'Simple Fade',
 					'slide'=>'Slide',
 					));
 
 $of_options[] = array( "name" => __('Feature Catgeory next to Slider','silvermag'),
					"desc" => __('Select category for slider. ','silvermag'),
					"id" => "feat_cat",
					"std" => "",
 					"type" => "select",
					"options" => $of_categories);					
   					
$of_options[] = array( "name" =>__('Single Posts','silvermag'),
					"type" => "heading");
$of_options[] = array( "name" => "General",
					"desc" => "",
					"id" => "introduction",
					"std" => __('<h3>Single Posts Settings</h3>','silvermag'),
					"icon" => true,
					"type" => "info");
										
$of_options[] = array( "name" => __('Show Featured image on posts','silvermag'),
					"desc" => '',
					"id" => "featured_image",
					"std" 		=> 1,
					"on" 		=> "Show",
					"off" 		=> "Hide",
					"type" 		=> "switch"	
						);
$of_options[] = array( "name" => __('Show Single Post Next/Prev navigation','silvermag'),
					"desc" => '',
					"id" => "posts_navigation",
					"std" 		=> 1,
					"on" 		=> "Show",
					"off" 		=> "Hide",
					"type" 		=> "switch"	
						);
$of_options[] = array( "name" => __('Show Breadcrumbs','silvermag'),
					"desc" => '',
					"id" => "posts_bread",
					"std" 		=> 1,
					"on" 		=> "Show",
					"off" 		=> "Hide",
					"type" 		=> "switch"	
						);				
 						
$of_options[] = array( "name" => __('Show related posts on posts','silvermag'),
					"desc" =>__('Check to Show related posts on posts','silvermag'),
					"id" => "posts_related",
					"std" => 1,
          			"folds" => 1,
					"on" 		=> "Show",
					"off" 		=> "Hide",
					"type" => "switch"
					); 	 	
$of_options[] = array( "name" => __('Show related posts by Category Wise or Tag Wise','silvermag'),
					"desc" =>'',
					"id" => "related_posts_tag",
 					"std" 		=> 1,
					"on" 		=> "Category",
					"off" 		=> "Tags",
					"type" 		=> "switch"	
						);
$of_options[] = array( "name" =>__('Enter Number of Posts','silvermag'),
					"desc" => '',
					"id" => "re_post_no",
					"fold" =>"posts_related",
					"std" => "3",
					"type" => "text");		
					
						
$of_options[] = array( "name" => __('Show Tags on posts','silvermag'),
					"desc" =>'',
					"id" => "posts_tags",
					"std" 		=> 1,
					"on" 		=> "Show",
					"off" 		=> "Hide",
					"type" 		=> "switch"	
						);						
$of_options[] = array( "name" => __('Show About Author Box ', 'silvermag'),
					"desc" => __('Select to show the Author Box in Single Posts', 'silvermag'),
					"id" => "shw_auth_box",
					"std" => 1,
          			"folds" => 1,
					"on" 		=> "Show",
					"off" 		=> "Hide",
					"type" => "switch"
					); 	
  

$of_options[] = array( "name" => "Social Shares",
					"type" => "heading");
$of_options[] = array( "name" => "General",
					"desc" => "",
					"id" => "introduction",
					"std" => __('<h3>Share Posts in Social Networks</h3>','silvermag'),
					"icon" => true,
					"type" => "info");
					
										
$show_hide=array('show'=>'Show','hide'=>'Hide');				

$of_options[] = array( "name" => __('Social Profile Icons ', 'silvermag'),
					"desc" => __('Select to enable/disable to show the social Icons in the Ticker side.', 'silvermag'),
					"id" => "social_icons",
					"std" 		=> 1,
					"on" 		=> "Enable",
					"off" 		=> "Disable",
					"type" 		=> "switch"	
					);
$of_options[] = array( "name" => __('Facebook', 'silvermag'),
                    "desc" => __('Add the URL you want and Facebook icon will appear.', 'silvermag'),
                    "id" => "si_fb",
                    "std" => "",
                    "type" => "text");						
$of_options[] = array( "name" => __('Twitter', 'silvermag'),
                    "desc" => __('Enter the Twitter Profile URL', 'silvermag'),
                    "id" => "si_tw",
                    "std" => "",
                    "type" => "text");
					
$of_options[] = array( "name" => __('Google+', 'silvermag'),
                    "desc" => __('Enter Google+ URL. </br>', 'silvermag'),
                    "id" => "si_gp",
                    "std" => "",
                    "type" => "text");
					
$of_options[] = array( "name" => __('YouTube', 'silvermag'),
                    "desc" => __('Enter YouTube URL', 'silvermag'),
                    "id" => "si_yt",
                    "std" => "",
                    "type" => "text");
					
$of_options[] = array( "name" => __('Vimeo', 'silvermag'),
                    "desc" => __('Enter Vimeo URL', 'silvermag'),
                    "id" => "si_vi",
                    "std" => "",
                    "type" => "text");

					
					
$of_options[] = array( "name" => __('Social Shares in Single Posts', 'silvermag'),
					"desc" => __('Select to enable/disable to show the social Shares in Single Posts.', 'silvermag'),
					"id" => "social_shares",
					"std" 		=> 1,
					"on" 		=> "Enable",
					"off" 		=> "Disable",
					"type" 		=> "switch"	
						);				
 						
$of_options[] = array( "name" => __('Twitter Consumer Key', 'silvermag'),
					"desc" => __('For Social Counter Widget', 'silvermag'),
					"id" => "twit_c_key",
					"std" => "",
					"type" => "text"
					 );
 $of_options[] = array( "name" => __('Twitter Consumer Secret', 'silvermag'),
						"desc" => __('For Social Counter Widget', 'silvermag'),
						"id" => "twit_c_secret",
						"std" => "",
						"type" => "text");
						
$of_options[] = array( "name" => __('Twitter Access Token', 'silvermag'),
						"desc" => __('For Social Counter Widget', 'silvermag'),
						"id" => "twit_a_token",
						"std" => "",
						"type" => "text");
						
 $of_options[] = array( "name" => __('Twitter Access Token secret', 'silvermag'),
						"desc" => __('For Social Counter Widget', 'silvermag'),
						"id" => "twit_a_token_secret",
						"std" => "",
						"type" => "text");
					
$of_options[] = array( "name" => __('Facebook Like', 'silvermag'),
					"desc" => __('Select to enable/disable to show the Facebook Like in Single Posts.', 'silvermag'),
					"id" => "share_fblike",
							"std" 		=> 1,
					"on" 		=> "Show",
					"off" 		=> "Hide",
					"type" 		=> "switch"	
						);				
 						
$of_options[] = array( "name" => __('Google +1 button', 'silvermag'),
					"desc" => __('Select to enable/disable to show the Google +1 button in Single Posts.', 'silvermag'),
					"id" => "share_gp",
					"std" 		=> 1,
					"on" 		=> "Show",
					"off" 		=> "Hide",
					"type" 		=> "switch"	
						);
$of_options[] = array( "name" => __('Twitter Tweet', 'silvermag'),
					"desc" => __('Select to enable/disable to show the Twitter Tweet button in Single Posts.', 'silvermag'),
					"id" => "share_tw",
									"std" 		=> 1,
					"on" 		=> "Show",
					"off" 		=> "Hide",
					"type" 		=> "switch"	
						);				
 						
$of_options[] = array( "name" => __('Pin it Button', 'silvermag'),
					"desc" => __('Select to enable/disable to show the Pin it Button in Single Posts.', 'silvermag'),
					"id" => "share_pin",
									"std" 		=> 1,
					"on" 		=> "Show",
					"off" 		=> "Hide",
					"type" 		=> "switch"	
						);				
 							
$of_options[] = array( "name" => __('LinkedIn  Button', 'silvermag'),
					"desc" => __('Select to enable/disable to show the LinkedIn in Single Posts.', 'silvermag'),
					"id" => "share_in",
								"std" 		=> 1,
					"on" 		=> "Show",
					"off" 		=> "Hide",
					"type" 		=> "switch"	
						);				
 												
  					
					
 
					
$of_options[] = array( "name" => __('Ad Banners','silvermag'),
					"type" => "heading");
$of_options[] = array( "name" => "General",
					"desc" => "",
					"id" => "introduction",
					"std" => __('<h3>Advertisements Settings</h3>','silvermag'),
					"icon" => true,
					"type" => "info");
										
$of_options[] = array( "name" => __('Header Ad',''),
					"desc" => __('Show Header Ad box','silvermag'),
					"id" => "head_ad_sel",
					"std" => 0,
          			"folds" => 1,
					"on" 		=> "Show",
					"off" 		=> "Hide",
					"type" => "switch"); 
$of_options[] = array( "name" => __('Upload Ad Banner Image','silvermag'),
					"desc" => __('Upload a 468x60 or 728x90 Banner Ad image. </br></br><li>If you want to show Banner, Upload Banner Ad Image and enter the Banner URL Below.</li><li>If you want to show Adsense like ads, then remove the Banner image and add the code below</li>','silvermag'),
					"id" => "head_ban_ad_img",
					"fold"=>"head_ad_sel",
					"std" => "",
					"type" => "media"); 					
$of_options[] = array( "name" => __('Enter AD Code or Banner AD URL','silvermag'),
					"desc" => __('Enter your Ad code like Adsense Scripts here OR Enter Banner AD URL only.</br></br>
					<li>If you want to show Adsense like ads, then add Ad code.</li><li>If you want to show Banner ad,then remove the Ad Code and add Banner URL</li>','silvermag'),
					"id" => "head_ad_code",
					"fold"=>"head_ad_sel",
					"std" => "",
					"type" => "textarea");  
 
 
 $of_options[] = array( "name" => __('Show Ad the in Beginning of Post','silvermag'),
					"desc" =>__('Enter your Ad code here. This will be added into the Post after the Post title.','silvermag'),
					"id" => "ptitle_below_ad_sel",
					"std" => 0,
          			"folds" => 1,
					"on" 		=> "Show",
					"off" 		=> "Hide",
					"type" => "switch");
$of_options[] = array( "name" => __('Upload Ad Banner Image','silvermag'),
					"desc" => __('Upload a Banner Ad image. </br></br><li>If you want to show Banner, Upload Banner Ad Image and enter the Banner URL Below.</li><li>If you want to show Adsense like ads, then remove the Banner image and add the code below</li>','silvermag'),
					"id" => "ptitle_below_ad_img",
					"fold"=>"ptitle_below_ad_sel",
					"std" => "",
					"type" => "media"); 					
$of_options[] = array( "name" => __('Enter AD Code or Banner AD URL','silvermag'),
					"desc" => __('Enter your Ad code like Adsense Scripts here OR Enter Banner AD URL only.</br></br>
					<li>If you want to show Adsense like ads, then add Ad code.</li><li>If you want to show Banner ad,then remove the Ad Code and add Banner URL</li>','silvermag'),
					"id" => "ptitle_below_ad_code",
					"fold"=>"ptitle_below_ad_sel",
					"std" => "",
					"type" => "textarea");  
 
 
 
  $of_options[] = array( "name" => __('Show Ad in the End of Post','silvermag'),
					"desc" => __('Enter your Ad code here. This will be added into the end of the post.','silvermag'),
					"id" => "pend_below_ad_sel",
					"std" => 0,
          			"folds" => 1,
					"on" 		=> "Show",
					"off" 		=> "Hide",
					"type" => "switch");
$of_options[] = array( "name" => __('Upload Ad Banner Image','silvermag'),
					"desc" => __('Upload a Banner Ad image. </br></br><li>If you want to show Banner, Upload Banner Ad Image and enter the Banner URL Below.</li><li>If you want to show Adsense like ads, then remove the Banner image and add the code below</li>','silvermag'),
					"id" => "pend_below_ad_img",
					"fold"=>"pend_below_ad_sel",
					"std" => "",
					"type" => "media"); 					
$of_options[] = array( "name" => __('Enter AD Code or Banner AD URL','silvermag'),
					"desc" => __('Enter your Ad code like Adsense Scripts here OR Enter Banner AD URL only.</br></br>
					<li>If you want to show Adsense like ads, then add Ad code.</li><li>If you want to show Banner ad,then remove the Ad Code and add Banner URL</li>','silvermag'),
					"id" => "pend_below_ad_code",
					"fold"=>"pend_below_ad_sel",
					"std" => "",
					"type" => "textarea"); 
					
 
 

					
$of_options[] = array( "name" => __('Theme Skins','silvermag'),
					"type" => "heading"); 	
$of_options[] = array( "name" => "General",
					"desc" => "",
					"id" => "introduction",
					"std" => __('<h3>Stylish Theme Skins</h3>','silvermag'),
					"icon" => true,
					"type" => "info");


$urlskn =  ADMIN_DIR . 'assets/images/skins/';
$of_options[] = array( "name" =>__('Theme Skins','silvermag'),
					"desc" =>'',
					"id" => "tptheme_skins",
					"std" => "default",
					"type" => "images",
					"options" => array(
						'default' => $urlskn . 'tpcrn_trans.png',
						'velvet' => $urlskn . 'tpcrn_trans.png',
						'darkblue' => $urlskn . 'tpcrn_trans.png',						
						'green' => $urlskn . 'tpcrn_trans.png',												
						'pink' => $urlskn . 'tpcrn_trans.png',
						'purple' => $urlskn . 'tpcrn_trans.png',
						'cappuccino' => $urlskn . 'tpcrn_trans.png',
						'turquoise' => $urlskn . 'tpcrn_trans.png',
						'orange' => $urlskn . 'tpcrn_trans.png'
					 )
					);							
$of_options[] = array( "name" => __('Typography','silvermag'),
					"type" => "heading");
					
$of_options[] = array( "name" => __('Typography Settings','silvermag'),
					"desc" => "",
					"id" => "typo_intro",
					"std" => "<h3>Typography Settings</h3>",
					"icon" => true,
					"type" => "info");					
$of_options[] = array( "name" => __('Body Font','silvermag'),
					"desc" => __('Specify the body font properties','silvermag'),
					"id" => "body_font",
					"std" => array('size' => '13px','face' =>'','style' => 'normal','color'=>'#666'),
					"type" => "typography");
$of_options[] = array( "name" => __('Top Menu Navigation','silvermag'),
					"desc" => __('Specify the Header Top Menu font properties','silvermag'),
					"id" => "headings_topmenu_font",
					"std" => array('size' => '13px','face' => 'Open Sans','style' => 'normal','color'=>''),
					"type" => "typography");
$of_options[] = array( "name" => __('Category Menu Navigation','silvermag'),
					"desc" => __('Specify the Category Menu Navigation font properties','silvermag'),
					"id" => "headings_catmenu_font",
					"std" => array('size' => '16px','face' => 'Oswald','style' => 'normal','color'=>''),
					"type" => "typography");	
$of_options[] = array( "name" => __('Blog and Magazine Category Title','silvermag'),
					"desc" => __('Specify the Blog and Magazine Category Title font properties','silvermag'),
					"id" => "blog_mag_title_font",
					"std" => array('size' => '14px','face' => 'default','style' => 'normal','color'=>''),
					"type" => "typography");
$of_options[] = array( "name" => __('HomePage Blog Post Title','silvermag'),
					"desc" => __('Specify the Blog  Post Title font properties','silvermag'),
					"id" => "blog_post_title_font",
					"std" => array('size' => '20px','face' => 'default','style' => 'normal','color'=>''),
					"type" => "typography");
$of_options[] = array( "name" => __('HomePage Magazine Post Title','silvermag'),
					"desc" => __('Specify the Magazine Post Title font properties','silvermag'),
					"id" => "mag_post_title_font",
					"std" => array('size' => '14px','face' => 'default','style' => 'normal','color'=>''),
					"type" => "typography");					
$of_options[] = array( "name" => __('Sidebar Widget Title','silvermag'),
					"desc" => __('Specify the Sidebar Widget Title font properties','silvermag'),
					"id" => "sb_widget_title_font",
					"std" => array('size' => '14px','face' => 'default','style' => 'normal','color'=>''),
					"type" => "typography");
$of_options[] = array( "name" => __('Footer Blocks Widget Title','silvermag'),
					"desc" => __('Specify the Footet Blocks Widget Title font properties','silvermag'),
					"id" => "fb_widget_title_font",
					"std" => array('size' => '21px','face' => 'default','style' => 'normal','color'=>''),
					"type" => "typography");					
					
$of_options[] = array( "name" => __('Typography Post elements Settings','silvermag'),
					"desc" => "",
					"id" => "typo_intro",
					"std" => "<h3>Typography Post elements</h3>",
					"icon" => true,
					"type" => "info");	
					
 
$of_options[] = array( "name" => __('Post Title','silvermag'),
					"desc" => __('Specify the Single Post Title font properties','silvermag'),
					"id" => "post_title_font",
					"std" => array('size' => '40px','face' => 'default','style' => 'normal','color'=>''),
					"type" => "typography");						
$of_options[] = array( "name" => __('Post Meta','silvermag'),
					"desc" => __('Specify the Single Post Title font properties','silvermag'),
					"id" => "post_meta_font",
					"std" => array('size' => '10px','face' => 'default','style' => 'normal','color'=>''),
					"type" => "typography");
$of_options[] = array( "name" => __('Post Content','silvermag'),
					"desc" => __('Specify the Post Content font properties','silvermag'),
					"id" => "post_content_font",
					"std" => array('size' => '13px','face' => 'default','style' => 'normal','color'=>''),
					"type" => "typography");
 
$of_options[] = array( "name" => __('Single Post Navigation','silvermag'),
					"desc" => __('Specify Single Post and comment navigation font properties','silvermag'),
					"id" => "post_navigation_font",
					"std" => array('size' => '14px','face' => 'Bitter','style' => 'normal','color'=>''),
					"type" => "typography");					
$of_options[] = array( "name" => __('Comments Block',''),
					"desc" => __('Specify the Comments Block content font properties','silvermag'),
					"id" => "post_comment_font",
					"std" => array('size' => '13px','face' => 'default','style' => 'normal','color'=>''),
					"type" => "typography");
$of_options[] = array( "name" => __('Typography Post Content Headings Settings','silvermag'),
					"desc" => "",
					"id" => "typo_intro",
					"std" => "<h3>Typography Post Content Headings</h3>",
					"icon" => true,
					"type" => "info");	

$of_options[] = array( "name" => __('H1 Headings','silvermag'),
					"desc" => __('Specify the H1 Headings font properties','silvermag'),
					"id" => "post_h1_font",
					"std" => array('size' => '32px','face' => 'default','style' => 'normal','color'=>''),
					"type" => "typography");

$of_options[] = array( "name" => __('H2 Headings','silvermag'),
					"desc" => __('Specify the H2 Headings font properties','silvermag'),
					"id" => "post_h2_font",
					"std" => array('size' => '28px','face' => 'default','style' => 'normal','color'=>''),
					"type" => "typography");

$of_options[] = array("name" => __('H3 Headings','silvermag'),
					"desc" => __('Specify the H3 Headings font properties','silvermag'),
					"id" => "post_h3_font",
					"std" => array('size' => '24px','face' => 'default','style' => 'normal','color'=>''),
					"type" => "typography");
					
$of_options[] = array( "name" => __('H4 Headings','silvermag'),
					"desc" => __('Specify the H4 Headings font properties','silvermag'),
					"id" => "post_h4_font",
					"std" => array('size' => '20px','face' => 'default','style' => 'normal','color'=>''),
					"type" => "typography");
					
$of_options[] = array( "name" => __('H5 Headings','silvermag'),
					"desc" => __('Specify the H5 Headings font properties','silvermag'),
					"id" => "post_h5_font",
					"std" => array('size' => '16px','face' => 'default','style' => 'normal','color'=>''),
					"type" => "typography");
					
$of_options[] = array( "name" => __('H6 Headings','silvermag'),
					"desc" => __('Specify the H6 Headings font properties','silvermag'),
					"id" => "post_h6_font",
					"std" => array('size' => '12px','face' => 'default','style' => 'normal','color'=>''),
					"type" => "typography");					


 					
$of_options[] = array( "name" => __('Styling','silvermag'),
					"type" => "heading");
$of_options[] = array( "name" => __('Site Styling Settings Page','silvermag'),
					"desc" => "",
					"id" => "typo_intro",
					"std" => "<h3>Site Styling Settings </h3>",
					"icon" => true,
					"type" => "info");					

					
$of_options[] = array( "name" =>  __('Body Background Color','silvermag'),
					"desc" => __('Pick a background color for the theme.','silvermag'),
					"id" => "body_background_color",
					"std" => "#fff",
					"type" => "color");
 
					
$of_options[] = array( "name" => __('Background Patterns','silvermag'),
					"desc" => '',
					"id" => "custom_bg",
					//"std" => $bg_images_url."bg10.png",
					"std" => "",
 					"type" => "tiles",
					"options" => $bg_images,
					);
 $of_options[] = array( "name" => __('Upload Background Pattern','silvermag'),
					"desc" => __('Upload a Custom background pattern.</br></br><b>Note:</b>If you want to show default background pattern then remove the custom Pattern','silvermag'),
					"id" => "custom_bg_upload",
					"std" => "",
 					"type" => "media"
 					);	
$of_options[] = array( "name" => __('Custom Pattern as Full Screen background', 'silvermag'),
					"desc" => __('To Show Custom Pattern as Full Screen background', 'silvermag'),
					"id" => "custom_full_bg_img",
					"std" => "no",
					"type" => "select",
					"options" => array('yes'=>'Yes','no'=>'No'));					
					
$of_options[] = array( "name" => __('Custom CSS','silvermag'),
                    "desc" => __('Quickly add some CSS to your theme by adding it to this block.','silvermag'),
                    "id" => "custom_css",
                    "std" => "",
                    "type" => "textarea");
					
					
$of_options[] = array( "name" => __('Backup Options','silvermag'),
					"type" => "heading");
$of_options[] = array( "name" => "General",
					"desc" => "",
					"id" => "introduction",
					"std" => __('<h3>Backup and Restore</h3>','silvermag'),
					"icon" => true,
					"type" => "info");
										
$of_options[] = array( "name" => __('Backup and Restore Options','silvermag'),
                    "id" => "of_backup",
                    "std" => "",
                    "type" => "backup",
					"desc" => __('You can use the two buttons below to backup your current options, and then restore it back at a later time. This is useful if you want to experiment on the options but would like to keep the old settings in case you need it back.','silvermag'),
					);
					
$of_options[] = array( "name" => __('Transfer Theme Options Data','silvermag'),
                    "id" => "of_transfer",
                    "std" => "",
                    "type" => "transfer",
					"desc" => __('You can tranfer the saved options data between different installs by copying the text inside the text box. To import data from another install, replace the data in the text box with the one from another install and click "Import Options".
						','silvermag'),
					);
					
	}
}
?>

<?php
/****************************************************************/
/*	Plugin Name: Banner Ad Widget	            	           */
/*	Description: The Widget will display Ads in your sidebar  */
/*	Author: ThemePacific Team								 */
/*	Author URI: http://themepacific.com						*/
/***********************************************************/

/**
 * Add function to widgets_init that'll load our widget.
 * @since 0.1
 */
add_action( 'widgets_init', 'silvermag_themepacific_sb_ad_widget' );
 /**
 * Register our widget.
 * 'Example_Widget' is the widget class used below.
 *
 * @since 0.1
 */
function silvermag_themepacific_sb_ad_widget() {
	register_widget( 'silvermag_themepacific_sb_Ad_Widget' );
}

/**
 * Example Widget class.
 * This class handles everything that needs to be handled with the widget:
 * the settings, form, display, and update.  Nice!
 *
 * @since 0.1
 */class silvermag_themepacific_sb_Ad_Widget extends WP_Widget {

	// Widget setup
	
	function silvermag_themepacific_sb_ad_Widget() {
$widget_ops = array('classname' => 'tpcrn_sb_ad_widget','description' => __( 'Widget will display Single sb Ad , in your sidebar.', 'silvermag' ));
$this->WP_Widget('tpcrn-sb-ad-widget', __( 'ThemePacific: sb Banner Ad', 'silvermag' ), $widget_ops);	
	}
	
	/**
* Display the widget
*/	
	function widget( $args, $instance ) {
		extract($args);
		
		$adcodevar = $instance['adcodevar'];
		
		echo $before_widget;
		
			echo '<div class="tpcrnbannersb tpcrnadvert">';
				echo $adcodevar;												
			echo '</div>'; 		

		echo $after_widget;
		
	}
	
	/**
	 * Update the widget settings.
	 */
	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['adcodevar'] = $new_instance['adcodevar'];
		return $instance;
	}
	// Widget form
	
	function form( $instance ) {
	$defaults = array( 'adcodevar' => '');
   				$instance = wp_parse_args( (array) $instance, $defaults );
	
		?>

	<p><small><?php _e('For Banner :</br> <strong>&lt;img src=&quot;BANNER-URL-HERE&quot;/&gt;</strong>','silvermag'); ?></small></p>
		<p><small><?php _e('For Banner With link :</br> <strong>&lt;a href=&quot;AD-LINK-HERE&quot;&gt;&lt;img src=&quot;BANNER-URL-HERE&quot;/&gt;&lt;/a&gt;</strong>','silvermag'); ?></small></p>
		<p><small><?php _e('For Banner With link open new windows :</br><strong> &lt;a href=&quot;AD-LINK-HERE&quot; target=&quot;_blank&quot;&gt;&lt;img src=&quot;BANNER-URL-HERE&quot;/&gt;&lt;/a&gt;</strong>','silvermag'); ?></small></p>	 
		 
		<p>
			<label for="<?php echo $this->get_field_id('adcodevar'); ?>"><?php _e('Enter sb Ad Code:','silvermag'); ?></label><br />
			
			<textarea class="widefat" rows="8" cols="20" id="<?php echo $this->get_field_id('adcodevar'); ?>" name="<?php echo $this->get_field_name('adcodevar'); ?>"><?php echo $instance['adcodevar']; ?></textarea>
		</p>
				
		<?php		
	
	}
	
}

?>
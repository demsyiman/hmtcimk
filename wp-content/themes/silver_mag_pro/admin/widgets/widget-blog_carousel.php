<?php
/************************************************************************
Plugin Name: Blog Carousel Widget
Description: The Widget will display your posts in HomePage carousel slider.
Author: RAJA CRN
Author URI: http://themepacific.com
*************************************************************************/
/**
 * Add function to widgets_init that'll load our widget.
 * @since 0.1
 */
 add_action( 'widgets_init', 'silvermag_themepacific_flex_carousel_widget' );
 /**
 * Register our widget.
 * 'Example_Widget' is the widget class used below.
 *
 * @since 0.1
 */
  function silvermag_themepacific_flex_carousel_widget() {
	register_widget( 'silvermag_themepacific_flex_carousel_widget');
}
/**
 * Example Widget class.
 * This class handles everything that needs to be handled with the widget:
 * the settings, form, display, and update.  Nice!
 *
 * @since 0.1
 */
class silvermag_themepacific_flex_carousel_widget extends WP_Widget {
  function silvermag_themepacific_flex_carousel_widget() {
  $widget_ops = array('classname' => 'tpcrn_flex_carousel_widget','description' => __( 'A Widget to dispaly Carousel slider', 'silvermag' ));
  $this->WP_Widget('tpcrn-flex-carousel-widget', __( 'ThemePacific : Carousel Widget', 'silvermag' ), $widget_ops);	
	}
	
/**
* Display the widget
*/	
  	function widget( $args, $instance ) {
		extract($args);
		$title = $instance['title'];
		$get_catego = $instance['get_catego'];
 		$getnumpost = $instance['getnumpost'];
		
		/* Before widget (defined by themes). */
		echo $before_widget;
 		?>
		
 	
		
	 <?php  global $post;
  			 $posts_slides = new WP_Query(array(
 				'showposts' => $getnumpost,
 				'cat' => $get_catego,
 			)); ?>
			 <!-- Begin BLOG Carousel Block -->
			
			<div class="head-span-lines"></div>
		 <h2 class="blogpost-tp-site-wrap-title"><a  style="color:<?php echo silvermag_themepacific_show_catcolor($content='block',$get_catego);?>" href="<?php echo get_category_link($get_catego); ?>"><?php if (get_cat_name($get_catego)) echo get_cat_name($get_catego);else echo $title;?></a> </h2>	
		 <div class='blog-carousel'>
	<?php if($posts_slides->have_posts()){?>
	 <div id="carousel-blocks">
		 <div class="blogflexcarousel carousel">
			 <ul class="flexcarousel-blocks slides clearfix">		
				 <?php while($posts_slides->have_posts()): $posts_slides->the_post(); ?>	
					<?php if(has_post_thumbnail()){ ?>
 					 <li> 					 		
 <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'sb-post-small-thumbnail'); ?>
 					<a href='<?php the_permalink(); ?>' title='<?php the_title(); ?>'><img  width="173" height="108" src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>" title="<?php the_title();?>" /> </a>
 
							<span class="mdate"> <?php the_time('F d, Y'); ?></span>
							
							<h4><a href="<?php the_permalink(); ?>"  title="<?php the_title(); ?>"><?php the_title(); ?></a></h4> 		
					 </li>			
					<?php } endwhile; ?>	
			 </ul>
		 </div>	
		 </div>	
	 <?php } ?>	
	</div>
		 <!-- end BLOG Carousel Block -->
		<?php	
	/* After widget (defined by themes). */
		echo $after_widget;		

	}
	
/**
	 * Update the widget settings.
	 */	function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['title'] = $new_instance['title'];
		$instance['get_catego'] = $new_instance['get_catego'];
		$instance['getnumpost'] = strip_tags( $new_instance['getnumpost']);
 		return $instance;
	}
	
	// Widget form
	
	function form( $instance ) {

				$defaults = array( 'title' => __('Category Name ', 'silvermag '),'getnumpost' => '8','get_catego' => 'all');
   				$instance = wp_parse_args( (array) $instance, $defaults ); ?>
	
 		<p>
			<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Enter category Name:', 'silvermag'); ?></label>
			<input class="widefat" style="width: 216px;" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $instance['title']; ?>" />
		</p>

 		
		<p>
			<label for="<?php echo $this->get_field_id('getnumpost'); ?>"><?php _e('Number of Posts to Show:','silvermag'); ?></label>
			<input id="<?php echo $this->get_field_id('getnumpost'); ?>" type="text" name="<?php echo $this->get_field_name('getnumpost'); ?>" value="<?php echo $instance['getnumpost'];?>"  maxlength="2" size="3" /> 
		</p>
			<p>
 			<label for="<?php echo $this->get_field_id('get_catego'); ?>">Filter by Category:</label> 
 			<select id="<?php echo $this->get_field_id('get_catego'); ?>" name="<?php echo $this->get_field_name('get_catego'); ?>" class="widefat categories" style="width:100%;">
 				<option value='all' <?php if ('all' == $instance['get_catego']) echo 'selected="selected"'; ?>>Select categories</option>
 				<?php $get_catego = get_categories('hide_empty=0&depth=1&type=post');  
 				 foreach($get_catego as $category) { ?>
 				<option value='<?php echo $category->term_id; ?>' <?php if ($category->term_id == $instance['get_catego']) echo 'selected="selected"'; ?>><?php echo $category->cat_name; ?></option>
 				<?php } ?>
 			</select>
 		</p>
		<?php
			
	}	

}
?>

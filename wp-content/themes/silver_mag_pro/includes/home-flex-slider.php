<?php 
global $themepacific_data;
if( $themepacific_data['feat_slide_trans'] == 'simplefade' ) 
$trans = 'animation: "fade",';
else $trans = 'animation: "slide",';
?>
<script type="text/javascript" charset="utf-8">
  jQuery(window).load(function() {
    jQuery('.flexslider.small').flexslider({
	<?php echo $trans;?>
 	controlNav: false,
	prevText: "",     
nextText: "", 
start: function(){
         jQuery('.flexslider.small').css("background", "none"); 
         jQuery('.flexslider.small').slideDown(); 
    },
	});
  });
</script>
 
	<!-- .fluid_container-->
<div class="fluid_container clearfix">
   <!-- #camera_wrap_2 -->
  <div class="flexslider small" id="camera_wrap_2">
	<?php
 		 if($themepacific_data['feat_slide_category'] != 'Select a category:') {
		$cat_id = get_cat_ID( $themepacific_data['feat_slide_category'] );
		$args = array('cat' => $cat_id,'showposts' => 5);
		$flex_posts = new WP_Query($args);  ?>
		<ul class="slides">
		  <?php while($flex_posts->have_posts()): $flex_posts->the_post(); ?>	
		
			<?php if(has_post_thumbnail()){ ?>
			
 				 <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'feat-slider'); ?>					
					<li>
						<figure> 				
						<span class="themepacific-cat-more" style="background:<?php echo silvermag_themepacific_show_catcolor($content='content');?>"> 
						<?php $category = get_the_category(); 
						$brecat_title = $category[0]-> cat_ID;
						$category_link = get_category_link($brecat_title);
						echo '<a class="tpcrn-cat-more" href="'. esc_url( $category_link ) . '">' . $category[0]->cat_name . '</a>';
						 ?></span>
					<a href="<?php the_permalink(); ?>">	 <img src="<?php echo $image[0]; ?>"/></a></figure>
 							<div class="slider-header-caption" style="background:rgba(5,84,163,0.6)">
							<time>  <span class="meta-date"> <?php the_time('F d, Y'); ?></span>
							</time>
								<div >
							   		<h2 style="color:white;opacity:1"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
 								</div>
 							</div>
					 </li>	
 		 
		    <?php } ?>
					
		  <?php endwhile; ?>
		</ul>
		  <?php wp_reset_query(); 

		  } else {  
    						
			$e_ids=explode(",",$themepacific_data['page_id_slider']);
			$flex_posts = new WP_Query( array('post_type' => array('post','page'), 'post__in' => $e_ids,'orderby' => 'post__in'  ) );
?>
		<ul class="slides">
		  <?php while($flex_posts->have_posts()): $flex_posts->the_post(); ?>	
		
			<?php if(has_post_thumbnail()){ ?>
			
 				 <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'feat-slider'); ?>
					
				<li>
						<figure> 				
						<span class="themepacific-cat-more" style="background:<?php echo silvermag_themepacific_show_catcolor($content='content');?>"> 
						<?php $category = get_the_category(); 
						$brecat_title = $category[0]-> cat_ID;
						$category_link = get_category_link($brecat_title);
						echo '<a class="tpcrn-cat-more" href="'. esc_url( $category_link ) . '">' . $category[0]->cat_name . '</a>';
						 ?></span>
					<a href="<?php the_permalink(); ?>">	 <img src="<?php echo $image[0]; ?>"/></a></figure>
 							<div class="slider-header-caption">
							<time>  <span class="meta-date"> <?php the_time('F d, Y'); ?></span>
							</time>
							   <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
 							</div>
					 </li>
 		 
		    <?php } ?>
					
		  <?php endwhile; ?>
		</ul>
		  <?php wp_reset_query();?>

<?php } ?>
		  
 
	</div><!-- /#camera_wrap_2 -->
</div>
<!-- /.fluid_container -->
 

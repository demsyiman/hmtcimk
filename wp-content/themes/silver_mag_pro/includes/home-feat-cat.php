<div class="featured-cat-top">
<!-- Begin category posts -->
 				<?php  	
				global $themepacific_data;
					$cat_id = get_cat_ID( $themepacific_data['feat_cat'] );
					$args = array('cat' => $cat_id,'showposts' => 3);
					$c_p = 0;					
					$tpcrn_feat_cat_query = new WP_Query($args);
					while ( $tpcrn_feat_cat_query -> have_posts() ) : $tpcrn_feat_cat_query-> the_post(); ?>
					<article   class="<?php if($c_p == 0) echo 'big'; if($c_p ==1) echo 'small'; if($c_p ==2) echo 'small last'; ?>">
						<div class="feat-cat-thumbnail" >
							<?php if($c_p == 0) {?>
						<span class="themepacific-cat-more" style="background:<?php echo silvermag_themepacific_show_catcolor($content='content');?>"> 
								<a class="tpcrn-cat-more" href="<?php echo get_category_link($cat_id ); ?>"><?php   echo get_cat_name($cat_id );?></a>
						</span> <?php } ?>
						<?php 
						
						
						if ( has_post_thumbnail() ) { if($c_p == 0) { ?>
						
							<a class="feat-post-thumbnail" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail('sb-post-big-thumbnail', array('title' => get_the_title())); ?></a>
						<?php  } elseif($c_p >=1){ ?>
						<a class="feat-post-thumbnail" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail('sb-post-small-thumbnail', array('title' => get_the_title())); ?></a>
						<?php
						} } else { ?>
							<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><img  src="<?php echo get_template_directory_uri(); ?>/images/default-image.png"  alt="<?php the_title(); ?>" /></a>
						<?php } ?>
						
						
						</div>						 
							<h3 style="<?php if ($c_p ==1 || $c_p==0) echo 'background:rgba(5,84,163,1)'; else echo 'background:rgba(5,84,163,0.6)'?>"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h3>
 					</article>
					<?php $c_p++; endwhile;  wp_reset_query(); ?>
 	<!-- End category posts -->
</div>
<!-- End feat cat top-->
﻿/*
    File Name: tpcrn_scripts.js
    Author : Raja CRN
	by ThemePacific
 */
jQuery(document).ready(function ($) {
  
 $('#sb-search').click(function() {
         $(this).addClass('sb-search-open');
		 $('.sb-search-input').focus();
     });
 
 $(document).mouseup(function (e){
    var container = $("#sb-search");

    if (!container.is(e.target) // if the target of the click isn't the container...
        && container.has(e.target).length === 0) // ... nor a descendant of the container
    {
        container.removeClass('sb-search-open');
    }
});
 
  	  $('.tabs_container2 .tab_content2:first').show();
        $('.tabs2 li:first').addClass('active');
        $.each($('.tabs2 li'),function(i,el){
            $(el).click(function(){
                $('.tabs_container2 .tab_content2').slideUp();
                $('.tabs_container2 .tab_content2').eq(i).slideDown();
                $('.tabs2 li').removeClass('active');
                $(this).addClass('active');
                 return false;
            });
         });
 /*Create the dropdown bases thanks to @chriscoyier*/
$("#catnav.secondary").append('<select class="resp-nav container">');
	/* Create default option "Go to..."*/
	$("<option />", {
	"selected": "selected",
	"value"   : "",
	"text"    : "Select Menu"
	}).appendTo("#catnav.secondary select");
/* Populate dropdowns with the first menu items*/
$("#catnav.secondary li ").each(function() {
	var href = $(this).children('a').attr('href');
	var text = $(this).children('a').html();
	var depth = $(this).parents('ul').length;
	text = (depth > 1) ?   ' &nbsp;&mdash; ' + text : text;
	text = (depth > 2) ?   '&nbsp;&nbsp;'+ text : text;
	text = (depth > 3) ?   '&nbsp;&nbsp;&nbsp;&mdash;'+ text : text;
	 $("#catnav.secondary select").append('<option value="' + href + '">' + text + '</option>');
});
/*make responsive dropdown menu actually work			*/
$("#catnav.secondary select").change(function() {
	window.location = $(this).find("option:selected").val();
});

/*cat nav menu*/
 
$("#catnav ul li:has(ul)").addClass("parent"); 
 $(".catnav li").hover(function () {
 $(this).has('ul').addClass("dropme");
 $(this).find('ul:first').css({display: "none"}).stop(true, true).slideDown(500);}, function () {
 $(this).removeClass("dropme");
 $(this).find('ul:first').css({display: "block"}).stop(true, true).slideUp(1000);
 });
 

  
  $('.tpcrn_share_l').click(function() {
     var NWin = window.open($(this).data('href'), '', 'height=600,width=600');
     if (window.focus) {NWin.focus();}return false;}); 
  //end
  
  $('.js-tp-site-wrap').mouseenter(function() {
//alert("System running.");
$(this).animate({
width: '+=17em',
}, 'fast');
$('.glass-body').toggleClass('black-col');
$('.glass-handle').toggleClass('black-col');
});
$('.js-tp-site-wrap').mouseleave(function() {
$(this).animate({
width: '-=17em',
}, 'fast');
$('.glass-body').toggleClass('black-col');
$('.glass-handle').toggleClass('black-col');
});
  
  
 
 
});

  /*Flex slider + Carousel*/

jQuery(window).load(function($) {
 
 var sec_left = jQuery("#tp-section-left").height();
 var sec_small = jQuery("#sidebar-small").height();
 if(sec_left > sec_small){
	jQuery("#sidebar-small").css("height", sec_left);
 }
 console.log(sec_left);
 console.log(sec_small);
 
   jQuery('.blogflexcarousel').flexslider({
    animationLoop: true,
    	animation: "slide",
				prevText: "",     
				nextText: "", 
				itemWidth: 174,
				itemMargin: 12,
				minItems: 2,                   
				maxItems: 6,                   
				move: 1,


  });
 
}); 
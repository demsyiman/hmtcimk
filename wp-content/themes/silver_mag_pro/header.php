<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>	
<!-- Meta info -->
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<!-- Title -->
<?php global $themepacific_data;?>
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
 <?php if($themepacific_data['custom_feedburner']) : ?>
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php echo esc_url($themepacific_data['custom_feedburner']); ?>" />
<?php endif; ?>
<?php if($themepacific_data['custom_favicon']): ?>
<link rel="shortcut icon" href="<?php echo esc_url($themepacific_data['custom_favicon']); ?>" /> <?php endif;  ?>
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<!-- CSS + jQuery + JavaScript --> 
<link href='//fonts.googleapis.com/css?family=Open+Sans:regular,bold' rel='stylesheet' type='text/css'/>
 
<link rel='stylesheet' href='<?php echo get_template_directory_uri(); ?>/css/ie8.css' type='text/css' media='all' />

  <!--[if  IE 9]>
<link rel='stylesheet' href='<?php echo get_template_directory_uri(); ?>/css/ie9.css' type='text/css' media='all' /> 
<![endif]-->
<?php 
	
 	wp_head();
	
?>
 
 	
 
<?php if($themepacific_data['tptheme_skins']!='default'){ if (!empty($themepacific_data['tptheme_skins'])) {?>
<link href="<?php echo get_template_directory_uri(); ?>/css/skins/<?php echo trim($themepacific_data['tptheme_skins']); ?>.css" rel="stylesheet" media="all" type="text/css" /> 
<?php } } ?>
</head>  

<body <?php body_class();?>> 

<!-- #tp-site-wrap -->	
<div id="tp-site-wrap" class="container clearfix"> 
 
	<!-- #CatNav -->  
	<div id="catnav" class="primary">	
		<?php //wp_nav_menu(array('theme_location' => 'topNav','container'=> '','menu_id'=> 'catmenu','menu_class'=> ' container clearfix','fallback_cb' => 'false','depth' => 3)); ?>
		
<?php if($themepacific_data['ticker_category'] != "0") { ?>
	
	<script>
	 jQuery(document).ready(function () {
	 
	 jQuery("#webticker").webTicker({
	 speed: 50, //pixels per second
		 startEmpty: false, //weather to start with an empty or pre-filled ticker
		});
	});
	</script>
 <div class="container">
		<span class="ticker-title"><?php _e('Breaking News','silvermag');?></span>
 		<ul id="webticker" >
				<?php 	 
				$ti_cat = get_cat_ID($themepacific_data['ticker_category']);
 					$tpcrn_tickerposts = new WP_Query(array(
						'showposts' => $themepacific_data['ticker_post_no'],
  						'cat' => $ti_cat ,	
 					));

							while( $tpcrn_tickerposts -> have_posts() ) : $tpcrn_tickerposts -> the_post(); ?>
									<li><span class="meta_sep"></span><a  class="news-item" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></li>
									
							<?php  endwhile; wp_reset_query(); ?>

		</ul>
		
	<?php if($themepacific_data['social_icons'] != "0") { ?>	
		<div class="social-top">
		  <div class="social-wrap">
		<?php if(!empty($themepacific_data['si_fb'])){ ?>
			   <a href="<?php echo $themepacific_data['si_fb'];?>"><i class="icon-facebook"></i></a>			   
		<?php } ?>
		<?php if(!empty($themepacific_data['si_gp'])){ ?>
			   <a href="<?php echo $themepacific_data['si_gp'];?>"><i class="icon-google-plus"></i></a>
		<?php } ?>		
		<?php if(!empty($themepacific_data['si_tw'])){ ?> 
			   <a href="<?php echo $themepacific_data['si_tw'];?>"><i class="icon-twitter"></i></a>
		<?php } ?>
		<?php if(!empty($themepacific_data['si_yt'])){ ?>
			   <a href="<?php echo $themepacific_data['si_yt'];?>"><i class="icon-youtube"></i></a>
		<?php } ?>
		<?php if(!empty($themepacific_data['si_vi'])){ ?>
			   <a href="<?php echo $themepacific_data['si_vi'];?>"><i class="icon-vimeo"></i></a>
		<?php } ?>
		  </div>
		</div>
	<?php } ?>	
 </div>
 <?php } ?>

		
	</div> 
	<!-- /#CatNav -->  
 <!-- /#Header --> 
<div id="tp-site-wrap-container"> 

<div id="header">	
	<div id="head-content" class="clearfix ">
 	 
			<!-- Logo --> 
			<div id="logo">   
				<?php if($themepacific_data['custom_logo'] !='') { 
				if($themepacific_data['custom_logo']) {  $logo = $themepacific_data['custom_logo']; 		
				} else { $logo = get_template_directory_uri() . '/images/logo.png'; 	
				} ?>  <a href="<?php echo esc_url( home_url( '/' ) );  ?>" title="<?php bloginfo( 'name' ); ?>" rel="home"><img src="<?php echo esc_url($logo); ?>" alt="<?php bloginfo( 'name' ) ?>" /></a>    
				<?php } else { ?>   
				<?php if (is_home()) { ?>     
				<h1><a href="<?php echo esc_url( home_url( '/' ) );  ?>" title="<?php bloginfo( 'name' ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1> <span><?php bloginfo( 'description' ); ?></span>
				<?php } else { ?>  
				<h2><a href="<?php echo esc_url( home_url( '/' ) );  ?>" title="<?php bloginfo( 'name' ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h2>  
				<?php } } ?>   
			</div>	 	
			<!-- /#Logo -->
 		
  					<!-- Header Ad -->
		<?php if($themepacific_data['head_ban_ad_img']) { ?>	
		<div id="header-banner468" class="clearfix">
					<a href="<?php echo $themepacific_data['head_ad_code']; ?>"> <img src="<?php echo $themepacific_data['head_ban_ad_img']; ?>" alt=""></a> 
			</div>
		<?php } else { ?>			
		<div id="header-banner468">
				<?php echo $themepacific_data['head_ad_code']; ?>
		</div>	
		<?php } ?>	
		<!-- /#Header Ad -->
	 	
 	</div>	
</div>
<!-- /#Header --> 

   <?php 
     if ( has_nav_menu('mainNav') ){ 
   ?>
	<!-- #CatNav secondary -->  
<div id="catnav" class="secondary">	
		<?php wp_nav_menu(array('theme_location' => 'mainNav','container'=> '','menu_id'=> 'catmenu','menu_class'=> 'catnav  container clearfix','fallback_cb' => 'false','depth' => 3,)); ?>
	
	<div id="sb-search" class="sb-search">
    <form method="get" action="<?php echo esc_url(home_url('/')); ?>">
        <input class="sb-search-input" required placeholder="<?php _e('Enter your search term...','silvermag');?>" type="search" value="" name="s" id="search" >
        <input class="sb-search-submit" type="submit" value="">
        <span class="sb-icon-search"></span>
    </form>
	</div>
</div> 
	<?php } ?>
	 <?php 
     if ( has_nav_menu('mainNav2') ){ 
   ?>
	<!-- /#CatNav  teritary-->  
		<div id="catnav" class="teritary">	
		<?php wp_nav_menu(array('theme_location' => 'mainNav2','container'=> '','menu_id'=> 'catmenu','menu_class'=> 'catnav  container clearfix','fallback_cb' => 'false','depth' => 0)); ?>
 
	</div>
	<?php } ?> 
 
 
 
 	<!--[if lt IE 8]>
		<div class="msgnote"> 
			Your browser is <em>too old!</em> <a rel="nofollow" href="http://browsehappy.com/">Upgrade to a different browser</a> to experience this site. 
		</div>
	<![endif]-->

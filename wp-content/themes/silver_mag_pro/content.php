<?php

global $themepacific_data;
 if( $themepacific_data['blog_style'] == 'traditional' ) {?>

<div class="blogposts-inner">
		<ul>	 		
			<li class="full-left clearfix">	
				<div class='magbig-thumb'>
					<span class="themepacific-cat-more" style="background:<?php echo silvermag_themepacific_show_catcolor($content='content');?>"> 
  				<?php $category = get_the_category(); 
				$brecat_title = $category[0]-> cat_ID;
				$category_link = get_category_link($brecat_title);
				echo '<a class="tpcrn-cat-more" href="'. esc_url( $category_link ) . '">' . $category[0]->cat_name . '</a>';
				 ?></span>
						 
					  <?php if ( has_post_thumbnail() ) { ?>
								<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="post-thumbnail">
								
								<?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'sb-post-big-thumbnail'); ?>
								<img src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>"  />
 		 
 							</a>
					  <?php } else { ?>
							<a href="localhost:50000/hmtcbaru/wp-admin/images/posko.jpg" rel="bookmark" title="<?php the_title(); ?>"><img   src="<?php echo get_template_directory_uri(); ?>/images/default-image.png" alt="<?php the_title(); ?>" /></a>
						<?php } ?>
						 
   				</div>
						 
				<div class="list-block clearfix">
					<h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3> 	
							<div class="post-meta-blog">
					<span class="meta_author"> <?php the_author_posts_link(); ?></span>
					<span class="meta_date"> <?php the_time('F d, Y'); ?></span>
					<span class="meta_comments">  <a href="<?php comments_link(); ?>"><?php comments_number(__( '0 Comment', 'silvermag' ), __( '1 Comment', 'silvermag' ), __( '% Comment', 'silvermag' )); ?></a></span>
					</div>
							
														
					<div class="maga-excerpt clearfix">
					<?php echo silvermag_themepacific_excerpt(18); ?>
					</div>	
 			</div>
		</li></ul>
	<br class="clear" />		
</div>
<?php } else {?>

					<div class="post-box-left <?php if($wp_query->current_post%2==0) {?> first-grid<?php }?> clearfix">
  						<article class="article-right">	
							<div class="article-header">   
								 <h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
							</div>
							<div class="post-meta-blog">
									<span class="meta_author"><?php the_author_posts_link(); ?></span>
									<span class="meta_date"> <?php the_time('F d, Y'); ?></span>
									<span class="meta_comments"> <a href="<?php comments_link(); ?>"><?php comments_number('0 Comment', '1 Comment', '% Comments'); ?></a></span>
								 </div>
 							<div class="magbig-thumb">
								<span class="themepacific-cat-more" style="background:<?php echo silvermag_themepacific_show_catcolor($content='content');?>"> 
								<?php $category = get_the_category(); 
								$brecat_title = $category[0]-> cat_ID;
								$category_link = get_category_link($brecat_title);
								echo '<a class="tpcrn-cat-more" href="'. esc_url( $category_link ) . '">' . $category[0]->cat_name . '</a>';
								 ?></span>
									<?php if ( has_post_thumbnail() ) { ?>
										<a href="<?php the_permalink(); ?>" class="post-thumbnail" title="<?php the_title(); ?>"><?php the_post_thumbnail('sb-post-big-thumbnail', array('title' => get_the_title())); ?></a>
									<?php } else { ?>
							<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title(); ?>"><img   src="<?php echo get_template_directory_uri(); ?>/images/default-image.png" alt="<?php the_title(); ?>" /></a>
						<?php } ?>
 							</div>

								<div class="article-content">
									<?php echo silvermag_themepacific_excerpt(12); ?>
								</div>
 
						</article>
 					</div>		
 			<?php if($wp_query->current_post%2== 1) {?>
 					 		 <div class='st_separator clearfix'></div>
							 <?php } ?>
							 
							 <?php } ?>

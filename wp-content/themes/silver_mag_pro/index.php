<?php get_header(); ?>
<!--#tp-section-wrap-->
<div id="tp-section-wrap" class="clearfix">
	
	<div class="featured-row clearfix">
	
	 <?php if($themepacific_data['offline_feat_slide'] != "0") { 
	 
	 
	 include_once('includes/home-flex-slider.php');
 		include_once('includes/home-feat-cat.php'); }
	 ?>
	 
	
	</div>
<!--#tp-section-left -->

	<div id="tp-section-left" class="eleven columns clearfix">	
 
 	
			<?php
				$hb_layout = $themepacific_data['homepage_blocks_content']['enabled'];

				if ($hb_layout):

				foreach ($hb_layout as $key=>$value) {

					switch($key) {
 
					 case 'hb_nor_blog':
					 ?>
						<div class="blogposts-wrap">
							<h2 class="blogpost-tp-site-wrap-title"><?php _e('Recent Posts', 'silvermag'); ?></h2>
						<?php
						
						
						if( $themepacific_data['blog_style'] == 'traditional' ) 				 						
 							get_template_part( 'includes/blog', 'loop' );
 						else  
							get_template_part( 'includes/grid', 'loop' );
							?>
					 </div>
					<?php
 					 break;	
					 
					 case 'hb_mag_1':
						dynamic_sidebar('Magazine Style Widgets');
						break;		
					case 'hb_mag_2':
					  dynamic_sidebar('Magazine Style Widgets2');
						break;	
					}

				}

				endif;						
						?>
   	</div>
 	<!-- /blocks col -->
 <?php get_sidebar();  ?>
 <?php get_footer(); ?>

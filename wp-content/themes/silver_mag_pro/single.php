<?php get_header(); ?>
<!-- #tp-section-wrap-->
<div id="tp-section-wrap" class="clearfix">
    <?php if (have_posts()) : while (have_posts()) : the_post();  ?> 
	
	<!-- /blocks Left -or -right -->
	<div id="tp-section-left" <?php post_class('eleven columns');?>>	 		
		
		<!-- .post-content-->
		<div class="post-content">
		
  				<?php 
			  if($themepacific_data['posts_bread'] == '1' ) {
			   silvermag_themepacific_breadcrumb(); 
			  }
			 ?>		
			<?php  
 			if($themepacific_data['featured_image']=='1'){
  			if(has_post_thumbnail()){
			?>
 				<figure class="feat-thumb">
 					<?php 
					 
						$singleImage = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full'); 
						$image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'feat-image'); 
					 ?>
  					<a rel="prettyPhoto" class="opac" href='<?php echo $singleImage[0]; ?>' title="<?php the_title(); ?>">
					<img src="<?php echo $image[0]; ?>" alt="<?php the_title(); ?>"   /></a>
				</figure>
				<!--/.feat-thumb-->
 			<?php } } ?>			 
		<!--/.post-outer -->
			<div class="post-outer clearfix">
			
 				<!--.post-title-->
 				  <div class="post-title"><h1 class="entry-title"><?php the_title(); ?></h1></div>
				  <!--/.post-title-->
 		<!--/#post-meta --> 
			<div class="post-meta-blog">
			<span class="meta_author vcard author"><span class="fn">  <?php the_author_posts_link(); ?></span></span>
			<span class="meta_date updated"> <?php the_time('F d, Y'); ?></span>
			<span class="meta_categ"><?php silvermag_themepacific_tpcrn_shw_category();?></span>


 
			<span class="meta_comments">  <a href="<?php comments_link(); ?>"><?php comments_number('0 Comment', '1 Comment', '% Comments'); ?></a></span>
 			<?php edit_post_link( __( 'Edit', 'silvermag' ), '<span class="edit-link">', '</span>' ); ?>
 			</div>
			<!--/#post-meta --> 
				  <!-- Ad post above -->
				  <?php if($themepacific_data['ptitle_below_ad_sel'] == "1") { ?>
 				 	<?php if($themepacific_data['ptitle_below_ad_img']) {?>
						<a href="<?php echo $themepacific_data['ptitle_below_ad_code']; ?>">
						<img src="<?php echo $themepacific_data['ptitle_below_ad_img']; ?>" border="0"></a>
 					<?php } else { ?>
 						<?php echo $themepacific_data['ptitle_below_ad_code']; ?>
					<?php } } ?>
			 <!-- .post_content -->
			  <div class = 'post_content entry-content'>
  					<?php the_content(); ?>
  					<div class="clear"></div>
				<!-- Ad post below -->
					<?php if($themepacific_data['pend_below_ad_sel'] == "1") { ?>
					<?php if($themepacific_data['pend_below_ad_img']) {?>
						<a href="<?php echo $themepacific_data['pend_below_ad_code']; ?>">
						<img src="<?php echo $themepacific_data['pend_below_ad_img']; ?>" border="0"></a>
 					<?php } else { ?>
 						<?php echo $themepacific_data['pend_below_ad_code']; ?>
                      <?php } }?>
 			 </div>	
			 <!-- /.post_content -->
					<?php silvermag_themepacific_wp_link_pages_tp(); ?>
   					<div class='clear'></div>
				
			</div>
		<!--/.post-outer -->
	<?php if($themepacific_data['posts_tags'] == '1'){ ?>
					<p class="post-tags">
						<strong><?php _e('TOPICS', 'silvermag'); ?> </strong><?php the_tags('',''); ?>					
						</p>
	<?php } ?>		
<?php  if( $themepacific_data['social_shares'] == '1'){  ?>		
 <div class="tpcrn-shr-post">
 <span class="head"><?php _e('Share', 'silvermag'); ?> </span>
 
 <div class="social-wrap-share">
 
       <?php if( $themepacific_data['share_tw'] == '1'){?>	
	   	   <a class="twtp tpcrn_share_l" data-href="http://twitter.com/share?url=<?php the_permalink();?>&amp;text=<?php the_title();?>" title="Share this on Twitter" href="javascript:void(0);" target="_blank"><i class="icon-twitter"></i></a>
         <?php } if( $themepacific_data['share_fblike'] == '1'){?> 
			   <a class="fbtp tpcrn_share_l" data-href="http://www.facebook.com/share.php?u=<?php the_permalink();?>" title="Share this on Facebook" href="javascript:void(0);" target="_blank"><i class="icon-facebook"></i></a>
		<?php }if( $themepacific_data['share_gp'] == '1'){?>
			   <a class="gptp tpcrn_share_l" data-href="https://plus.google.com/share?url=<?php the_permalink();?>" title="Share this on Google+" href="javascript:void(0);" target="_blank"><i class="icon-google-plus"></i></a>
 		<?php }if( $themepacific_data['share_pin'] == '1'){?>
		 <a class="pintp tpcrn_share_l" data-href="http://pinterest.com/pin/create/button/?url=<?php the_permalink();?>&amp;media=<?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'feat-image'); echo $image[0]; ?>&amp;description=<?php the_title();?>" title="Share this on Pinterest" href="javascript:void(0);" target="_blank"><i class="icon-pinterest"></i></a>
		<?php }if( $themepacific_data['share_in'] == '1'){?>
			   <a class="intp tpcrn_share_l" data-href="http://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink();?>&amp;summary=<?php the_title();?>" title="Share this on LinkedIn" href="javascript:void(0);" target="_blank"><i class="icon-linkedin"></i></a>

		<?php } ?>
 
	</div>
</div> <!-- .share-post -->
<?php } ?>
 		</div>
		<!-- post-content-->
 
			 <?php if($themepacific_data['posts_navigation'] == '1'){ ?>
		 		<!-- .single-navigation-->
					<div class="singlenavig clearfix">
	<div class="singlenavig-prev clearfix"><?php previous_post_link('%link', __(' <span class="arrow icon-arrow-left"></span><span class="singlenavig-prev-span"><span class="lnk">Previous</span><span class="title"> %title </span></span>','silvermag')); ?>
	</div>
					
					
<div class="singlenavig-next clearfix"><?php next_post_link('%link', __( '<span class="singlenavig-next-span"><span class="lnk">Next</span><span class="title">%title</span></span><span class="arrow icon-uniE616">','silvermag') ); ?>
			</div>
					 
				</div>
				<!-- /single-navigation-->
			<?php } ?>
 	
 			
			
		<?php if($themepacific_data['shw_auth_box'] == '1'){ ?>	
			<div class="author-content">
			<h3><?php _e('About', 'silvermag'); ?> <?php the_author_meta( 'nickname' ); ?></h3>
					<div id="tpcrn_author" class="author-gravatar-head clearfix">
 						<?php echo get_avatar(get_the_author_meta('email'),'80' ); ?>
					 	
							<div class="author_desc"><p><?php the_author_meta( 'description' ); ?></p></div>
 
						</div>
					</div>
			
			<?php } ?>
			
 <?php if( $themepacific_data['posts_related'] == '1'){
				 	 get_template_part( 'includes/single', 'related' );
					 }
					?>
   					<?php comments_template(); ?>
 				<?php endwhile; endif; ?>
			
			</div>
			<!-- /blocks Left-->
 			
<?php  get_sidebar(); ?>
			
<?php get_footer(); ?>
